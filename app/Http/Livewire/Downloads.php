<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Ride;
use App\Models\Vehicle;

class Downloads extends Component
{

    public function render()
    {
        return view('downloads.downloads');
    }

}
