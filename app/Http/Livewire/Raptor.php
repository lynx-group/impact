<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Ride;
use App\Models\Vehicle;

class Raptor extends Component
{

    public function render()
    {
        return view('raptor.raptor');
    }

}
