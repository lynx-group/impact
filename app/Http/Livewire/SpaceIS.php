<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Ride;
use App\Models\Vehicle;

class SpaceIS extends Component
{

    public function render()
    {
        return view('space.infrastructure');
    }

}
