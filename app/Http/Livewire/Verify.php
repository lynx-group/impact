<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Laravel\Fortify\Fortify;
use App\Models\User;
use App\Models\DiscordConnection;
use App\Models\DiscordVerification;
use Laravel\Fortify\Http\Controllers\AuthenticatedSessionController;

class Verify extends Component
{
    public $step;
    public $did;
    public $token;
    public $gcap;
    public $captcha;
    public $test;
    public $blacklisted = array();

    protected $rules = [
        'captcha' => 'required|captcha',
    ];

    public function mount($id)
    {
        $this->did = $id;
        $this->step = 2;

        // ? Check if the user is connected to a impact account
        if(DiscordConnection::where('discord_id', $this->did)->get()->count() == 1) {

            // ? User has already verified
            if(DiscordVerification::where('discord_id', $this->did)->get()->count() == 1) {
                $this->token = DiscordVerification::where('discord_id', $this->did)->first()->token;
                $this->step = 1;
            } else {
                if(in_array($this->did, $this->blacklisted)) {
                    $this->step = 2;
                } else {
                    $token = mt_rand(100000, 999999);
                    $verification = new DiscordVerification;
                    $verification->discord_id = $this->did;
                    $verification->token = $token;
                    $verification->save();
                    $this->token = $token;
                    $this->step = 1;
                }
            }
        }
    }

    public function render()
    {
            // ! User is not registered to a impact account
            if(DiscordVerification::where('discord_id', $this->did)->get()->count() == 1) {
                $this->token = DiscordVerification::where('discord_id', $this->did)->first()->token;
                $this->step = 1;
            }

        return view('auth.livewire.verify')->with(['id' => $this->did]);

    }

    public function viewreset() 
    {
        $this->step = 0;
    }

    public function gettoken() {

        $this->validate();
        if(in_array($this->did, $this->blacklisted)) {
            $this->step = 2;
        } else {
            $token = mt_rand(100000, 999999);
            $verification = new DiscordVerification;
            $verification->discord_id = $this->did;
            $verification->token = $token;
            $verification->save();
            $this->token = $token;
            $this->step = 1;
        }
        
        $this->token = mt_rand(100000, 999999);
    }
}
