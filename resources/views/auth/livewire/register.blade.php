<main class="page login-page">
<div class="alert alert-warning text-white bg-warning shadow-none" role="alert" style="margin: 0px;border:none;border-radius:0px;background-color:#2b2f31 !important;"><p class="text-danger" style="padding:0px;margin:0px;"><i class="far fa-exclamation-triangle" style="margin-right:10px;"></i> CEO Tek Xylo, might be getting injected with venom today morning at 9:30AM! This is extremely dangerous do not attempt! <a href="#learnmore" style="text-decoration:none;color:#fff;">Learn More</a></p></div>
    
   <section class="clean-block dark py-4" style="min-height:600px;">
      <div class="container">
         <div class="card" style="    max-width: 500px;    margin: auto;
            padding: 20px;border-radius:25px;">
            <div class="card-body">
               
            <form method="POST" action="/register">
                      @csrf
                           <h2 class="text-primary text-center">Register</h2>
                           <center><p style="text-center align-center">Create your Impact Account.</p></center>
                            <input id="name" type="text" placeholder="Name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <br>
                            <input id="email" type="email" placeholder="E-Mail Address" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <br>
                            <input id="password" type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <br>
                            <input id="password-confirm" type="password" placeholder="Confirm Password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            <!--<div class="form-group row">
                               <div class="col-md-6 offset-md-4">
                                   <div class="form-check">
                                       <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                       <label class="form-check-label" for="remember">
                                           {{ __('Remember Me') }}
                                       </label>
                                   </div>
                               </div>
                               </div>-->
                            <br>
                            <button type="submit" class="btn btn-primary" style="width:100%;margin-bottom:10px;">
                            {{ __('Register') }}
                            </button>
                            @if (Route::has('password.request'))
                            <!--<a class="btn btn-link" href="{{ route('password.request') }}">
                               {{ __('Forgot Your Password?') }}
                               </a>-->
                            @endif

                                                       <center><br>
                               <p>Or sign in with Google</p>
                            <img class="mb-3" src="https://media.discordapp.net/attachments/760701188030857223/765223635572621372/googlelogo_color_272x92dp.png" alt="" width="72">
                           </center>
                   </form>

            </div>
         </div>
      </div>
   </section> 
</main>