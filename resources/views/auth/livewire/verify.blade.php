<main class="page login-page">
   <section class="clean-block dark py-4" style="min-height:600px;">
      <div class="container">
         @if($step == 0)           
         <div class="card" style="    padding: 20px;max-width: 500px;    margin: auto;border-radius:25px;">
            <div class="card-body">
               

               <form wire:submit.prevent="gettoken">
               @csrf
               <h2 class="text-primary text-center">Verify</h2>
               <center><p style="text-center align-center">Please verify that you are not a bot.</p></center>
               <input type="hidden" id="discord_id" name="discord_id" value="{{$id}}">
               <style>
                  .text-center {
                     text-align: center;
                  }

                  .g-recaptcha {
                     display: inline-block;
                  }
               </style>
               <div style="text-align: center;">
               <div class="g-recaptcha" id="feedback-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY')  }}"></div>
               </div><br>
               @if ($errors->any())
               <p class="text-center text-danger"><i class="far fa-times-hexagon" style=""></i> Verification Failed!</p>
               @endif 
               <button class="btn btn-primary btn-block" type="submit">Verify</button><br>
               </form>


            </div>
         </div>
         @endif

         @if($step == 1)
         <div class="card" style="    max-width: 500px;    margin: auto;
            padding: 40px;border-radius:25px;">
            <div class="card-body">
               
            
            <center>
            <div class="user-avatar" style="display: block;margin-left: auto;margin-right: auto;background-image: url('https://media.discordapp.net/attachments/756086069275131945/769594596077862952/impact-discord.png?width=468&height=468');width:75px;height:75px;"></div>

            <br><h5 class="text-center text-success"><i class="fad fa-badge-check" style=""></i> You are now verified!</h5>
            <h3 style="letter-spacing: 8px;">{{$token}}</h3>
            <p class="text-center">Welcome to the Impact Community you may now close this page and head to discord to type your authentication code.</p>
            
            </center>


            </div>
         </div>
         @endif

         @if($step == 2)
         <div class="card" style="    max-width: 500px;    margin: auto;
            padding: 40px;border-radius:25px;">
            <div class="card-body">
               
            
            <center>
            <!--<div class="user-avatar" style="display: block;margin-left: auto;margin-right: auto;background-image: url('https://media.discordapp.net/attachments/756086069275131945/769594596077862952/impact-discord.png?width=468&height=468');width:75px;height:75px;"></div>

            <br><h5 class="text-center text-danger"><i class="far fa-times-hexagon" style=""></i> Verification Blacklist!</h5>
            <p class="text-center">Seems you've been blacklisted from the Impact Offroading Network, This means you'll lose access to joining the impact community or applications and creating other accounts. <br><strong>This action cannot be reversed by admins.</strong></p>
            <button class="btn btn-danger btn-block" wire:click="viewreset" disabled>Try Again</button><br>
            {{$test}}
            </center>-->
            <center>
            <div class="user-avatar" style="display: block;margin-left: auto;margin-right: auto;background-image: url('https://media.discordapp.net/attachments/756086069275131945/771035710622531634/impact-discord.png');width:75px;height:75px;"></div>

            <br><h5 class="text-center text-danger"><i class="far fa-exclamation-triangle" style=""></i> Verifications Disabled!</h5>
            <p class="text-center"><strong>Payment Issues with Impact</strong><br> We have disabled verification process for Impact due to a payment issue. Please contact an administrator of Impact in the #verify channel!<br><br><strong>Try Again Later.</strong></p>
            {{$test}}
            </center>

            </div>
         </div>
         @endif
      </div>
   </section>
</main>
<footer class="page-footer dark">
   <div class="container">
      <div class="row">
         <div class="col-sm-3">
            <h5>Get started</h5>
            <ul>
               <li><a href="#">Home</a></li>
               <li><a href="#">Sign up</a></li>
               <li><a href="#">Downloads</a></li>
            </ul>
         </div>
         <div class="col-sm-3">
            <h5>About us</h5>
            <ul>
               <li><a href="#">Company Information</a></li>
               <li><a href="#">Contact us</a></li>
               <li><a href="#">Reviews</a></li>
            </ul>
         </div>
         <div class="col-sm-3">
            <h5>Support</h5>
            <ul>
               <li><a href="#">FAQ</a></li>
               <li><a href="#">Help desk</a></li>
               <li><a href="#">Forums</a></li>
            </ul>
         </div>
         <div class="col-sm-3">
            <h5>Legal</h5>
            <ul>
               <li><a href="#">Terms of Service</a></li>
               <li><a href="#">Terms of Use</a></li>
               <li><a href="#">Privacy Policy</a></li>
            </ul>
         </div>
      </div>
   </div>
   <div class="footer-copyright">
    <p>© 2021 Copyrighted by Impact Offroading | Created & Designed by : Lynx Group <img src="/lynx.svg" alt="Italian Trulli" class="align-center text-center" style="width:25px;"></p>   
   </div>
</footer> 