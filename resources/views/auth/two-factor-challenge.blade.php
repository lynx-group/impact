@extends('layouts.app')

@section('content')
<main class="page login-page">
   <section class="clean-block dark py-4" style="min-height:600px;">
      <div class="container">
         <div class="card" style="    max-width: 500px;    margin: auto;
            padding: 20px;border-radius:25px;">
            <div class="card-body">
               
            <form method="POST" action="/two-factor-challenge">
                      @csrf
                           <h2 class="text-primary text-center"><i class="fal fa-fingerprint text-primary" style="font-size:40px;margin-bottom:15px;"></i><br> Two Factor Authentication</h2>
                           <center><p style="text-center align-center">Please authenticate your account.</p></center>
                            <input id="code" type="text" placeholder="Two Factor Code" class="form-control @error('name') is-invalid @enderror" name="code" value="" required autocomplete="code" autofocus>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <br>
                       
                            <button type="submit" class="btn btn-primary" style="width:100%;margin-bottom:10px;">
                            {{ __('Unlock') }}
                            </button>
                   </form>

            </div>
         </div>
      </div>
   </section> 
</main>



<!--<div class="container py-4">
<div class="container">
   <div class="row justify-content-center">
      <div class="col-md-6">
         <div class="card">
            <div class="card-body">
               <form method="POST" action="/two-factor-challenge">
                  @csrf
                  <div class="row justify-content-center">
                     <div class="col-md-10">
                        <center>
                           <br>
                           <i class="fad fa-fingerprint text-primary" style="font-size:40px;margin-bottom:15px;"></i>
                        <h5>Two Factor Authentication</h5>
                        <br>
                        </center>

                        <input id="code" name="code" type="text" placeholder="Two Factor Code" class="form-control @error('code') is-invalid @enderror" required>
                        <br>
                        <button type="submit" class="btn btn-primary" style="width:100%;margin-bottom:10px;">
                        {{ __('Unlock') }}
                        </button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
</div>-->
@endsection
