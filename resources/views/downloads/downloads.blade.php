<main class="page login-page">
<div class="alert alert-warning text-white bg-warning shadow-none" role="alert" style="margin: 0px;border:none;border-radius:0px;background-color:#2b2f31 !important;"><p class="text-warning" style="padding:0px;margin:0px;"><i class="far fa-ghost" style="color:#fff;margin-right:10px;"></i> Happy Halloween from the Impact Team & Lynx Group! We hope you wont be tricked.</p></div>
   
<div class="highlight-blue">
        <div class="container">
            <div class="intro">
                <h2 class="text-center">Downloads</h2>
                <p class="text-center">Download the Impact Software on your devices or download additional files here.</p>
            </div>
        </div> 
    </div>
   <section class="clean-block dark py-4" style="min-height:600px;">
      <div class="container">    



         <p style="padding:0px;margin:0px;font-size:25px;" class=""><i class="far fa-truck-monster"></i> <strong>Impact Hub</strong></p><hr>
         <div class="card" style="margin: auto;border-radius:25px;">
            <div class="card-body">
               
                  <div class="col-sm-12">
                  <p style="padding:0px;margin:0px;font-size:25px;" class="text-primary"><i class="fab fa-windows"></i> <strong>Download Impact Hub for Windows 10</strong></p>
                  <p><strong>Download Unavailable!</strong></p>
                  </div>
            </div>
         </div>

        <br><div class="card" style="margin: auto;border-radius:25px;">
            <div class="card-body">
               
                  <div class="col-sm-12">
                  <p style="padding:0px;margin:0px;font-size:25px;" class="text-primary"><i class="fab fa-ubuntu"></i> <strong>Download Impact Hub for Linux</strong></p>
                  <p><strong>Download Unavailable!</strong></p>
                  </div>
            </div>
         </div>

         <br><div class="card" style="margin: auto;border-radius:25px;">
            <div class="card-body">
               
                  <div class="col-sm-12">
                  <p style="padding:0px;margin:0px;font-size:25px;" class="text-primary"><i class="fab fa-android"></i> <strong>Download Impact Hub for Android</strong></p>
                  <p><strong>Download Unavailable!</strong></p>
                  </div>
            </div>
         </div>
         <br><p style="padding:0px;margin:0px;font-size:25px;" class=""><i class="far fa-cloud-download"></i> <strong>Additional Downloads</strong></p><hr>
         <br><div class="card" style="margin: auto;border-radius:25px;">
            <div class="card-body">
               
                  <div class="col-sm-12">
                  <p style="padding:0px;margin:0px;font-size:25px;" class="text-primary"><i class="fas fa-file-pdf"></i> <strong>Lynx API Documentation</strong></p>
                  <p><strong>LynxAPI-Documentation.pdf</strong></p>
                  <a href="" class="btn btn-primary" style="border-radius:25px;">Download</a>
                  </div>
            </div>
         </div>

      </div>
   </section>
</main>
