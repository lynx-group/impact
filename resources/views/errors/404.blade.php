
@extends('layouts.app')

@section('content')
<main class="page login-page">
   <section class="clean-block dark py-4" style="min-height:600px;">
      <div class="container">       
         <div class="card" style="margin: auto;border-radius:25px;">
            <div class="card-body">
               
                  <div class="col-sm-12">
                  <p style="padding:0px;margin:0px;font-size:25px;" class="text-danger"><img class="mb-3" src="/dirtbike.svg" alt="" width="50" style="margin-top:10px;"> <strong>Page Not Found</strong></p>
                  <p><strong>Looks like you got lost!</strong><br>Seems you might want to call your buddys for help. The page you're after no longer exists.</p>
                  <a href="/" class="btn btn-danger" style="border-radius:25px;">Go Home</a>

            </div>
         </div>
      </div>
   </section>
</main>

@endsection