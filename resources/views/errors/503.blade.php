

@extends('layouts.app')

@section('content')
<main class="page login-page">
   <section class="clean-block dark py-4" style="min-height:600px;">
      <div class="container">       
         <div class="card" style="margin: auto;border-radius:25px;">
            <div class="card-body">
               
                  <div class="col-sm-12">
                  <p style="padding:0px;margin:0px;font-size:25px;" class="text-danger"><img class="mb-3" src="/dirtbike.svg" alt="" width="50" style="margin-top:10px;"> <strong>Buissness Closure</strong></p>
                  <p><strong class="text-danger"><i class="far fa-times-circle text-danger"></i> Impact is currently closed.</strong><br>Impact is currently closed due to operation of low staff members or issues with our internal systems. This could take a few days before we restore our services.<br>Estimated Time : <strong>2 years</strong><br><br><strong style="font-size:25px;">What's happening?</strong><br><strong class="text-danger"><i class="far fa-times-circle text-danger"></i> Low staff member count</strong><br><strong class="text-danger"><i class="far fa-times-circle text-danger"></i> Tek Xylo is no longer with us</strong><br></p>

            </div>
         </div>
      </div>
   </section>
</main>
@endsection