<div class="alert alert-warning text-white bg-warning shadow-none" role="alert" style="margin: 0px;border:none;border-radius:0px;background-color:#2b2f31 !important;"><p class="text-danger" style="padding:0px;margin:0px;"><i class="far fa-exclamation-triangle" style="margin-right:10px;"></i> CEO Tek Xylo, might be getting injected with venom today morning at 9:30AM! This is extremely dangerous do not attempt! <a href="#learnmore" style="text-decoration:none;color:#fff;">Learn More</a></p></div>
    
<div class="container-fluid gedf-wrapper py-4">
<div class="card" style="border-radius: 25px;padding:0px;">
<div class="card-body" style="padding:0px;">
<ul class="nav nav-pills nav-fill">
  <li class="nav-item">
    <a wire:click="setPage('home')" href="#" class="nav-link @if($active == 'home') active @else text-light @endif" style="border-radius:25px;text-decoration:none;"> <i class="far fa-home" style="margin-right:5px;"></i> Home</a>
  </li>
  <li class="nav-item">
    <a class="nav-link text-light" href="#" style="text-decoration:none;"> <i class="far fa-shopping-cart" style="margin-right:5px;"></i> Marketplace</a>
  </li>
  <li class="nav-item">
    <a class="nav-link text-light" href="#" style="text-decoration:none;"> <i class="far fa-camera" style="margin-right:5px;"></i> Creators</a>
  </li>
  @guest
                @else
  <li class="nav-item">
    <a wire:click="setPage('history')" href="#" class="nav-link @if($active == 'history') active @else text-light @endif" style="border-radius:25px;text-decoration:none;"> <i class="far fa-history" style="margin-right:5px;"></i> Your Rides</a>
  </li>
  <li class="nav-item">
    <a wire:click="setPage('vehicles')" class="nav-link @if($active == 'vehicles') active @else text-light @endif" href="#" style="border-radius:25px;text-decoration:none;"> <i class="far fa-truck-monster" style="margin-right:5px;"></i> Your Vehicles</a>
  </li>
  <li class="nav-item">
    <a class="nav-link text-light" href="#" style="text-decoration:none;"> <i class="far fa-map-marker-alt" style="margin-right:5px;"></i> Trails</a>
  </li>
  <li class="nav-item">
    <a class="nav-link text-light" href="/account/settings" style="text-decoration:none;"> <i class="far fa-sliders-v-square" style="margin-right:5px;"></i> Account Settings</a>
  </li>
  @endif
</ul>
</div>
</div><br>
        <div class="row">
        @guest
                @else
            <div class="col-md-3">
                <div class="card" style="border-radius: 25px;">
                    <div class="card-body">
                        <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <div class="user-avatar" style="background-image: url('https://media.discordapp.net/attachments/756086069275131945/770741322965188608/lynx-github.png');width:75px;height:75px;"></div>
                            <div class="h5" style="margin-top:10px;">{{Auth::user()->name}}</div>
                        </li>
                        <li class="list-group-item">
                            <div class="h6 text-muted">Total Rides</div>
                            <div class="h5">0</div>
                        </li>
                        <li class="list-group-item">
                            <div class="h6 text-muted">Vehicles</div>
                            <div class="h5">0</div>
                        </li>
                        <!--<li class="list-group-item">Lives in Shediac, NB</li>-->
                    </ul>
                    </div>
                </div>
            </div>
            @endif
            @guest
            <div class="col-md-9 gedf-main">
                @else
            <div class="col-md-6 gedf-main">
            @endif

                @if($active == 'home')
                @guest
                @else
                <!--- \\\\\\\Post-->
                <div class="card gedf-card" style="border-radius: 25px;">
                    <div class="card-header" style="border:none;">
                        <div class="d-flex justify-content-between align-items-center py-2">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="mr-2">
                                    <img class="rounded-circle" width="45" src="https://media.discordapp.net/attachments/756086069275131945/770741322965188608/lynx-github.png" alt="">
                                </div>
                                <div class="ml-2">
                                    <div class="h5 m-0">{{Auth::user()->name}}</div>
                                    <div class="h7 text-muted">{{ "@" . strtolower(str_replace(" ", "", Auth::user()->name)) }}</div>
                                </div>
                            </div>
                            <!--<div>
                                <div class="dropdown">
                                    <button class="btn btn-link dropdown-toggle" type="button" id="gedf-drop1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="gedf-drop1">
                                        <div class="h6 dropdown-header">Configuration</div>
                                        <a class="dropdown-item" href="#">Save</a>
                                        <a class="dropdown-item" href="#">Hide</a>
                                        <a class="dropdown-item" href="#">Report</a>
                                    </div>
                                </div>
                            </div>-->
                        </div>

                    </div>
                    <div class="card-body" style="padding-top: 0px;">
                        <div class="tab-content" id="myTabContent">
                                <div class="form-group">
                                    <label class="sr-only" for="message">post</label>
                                    <textarea class="form-control" id="message" rows="3" placeholder="Going riding?" style="border-radius: 25px;resize: none;"></textarea>
                                </div>
                        </div>
                        <div class="row">
                        <div class="col">
                            <a href=""><i class="fal fa-images" style="font-size:25px;margin-left:5px;margin-right:5px;"></i></a>
                            <a href=""><i class="fal fa-video" style="font-size:25px;margin-left:5px;margin-right:5px;"></i></a>
                            <a href=""><i class="fal fa-location" style="font-size:25px;margin-left:5px;margin-right:5px;"></i></a>
                            <a href=""><i class="fal fa-truck-monster" style="font-size:25px;margin-left:5px;margin-right:5px;"></i></a>
                        </div>
                        <div class="col"><button type="submit" class="btn btn-primary float-right" style="border-radius: 25px;width:50%">Post</button></div>
                        </div>
                                

                    </div>
                </div>
                <br>
                <!-- Post /////-->
                @endif

                <!--- \\\\\\\Post-->
                <div class="card gedf-card" style="border-radius: 25px;">
                    <div class="card-header" style="border:none;">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="d-flex justify-content-between align-items-center">
                            <div class="mr-2">
                                    <img class="rounded-circle" width="45" src="https://cdn.discordapp.com/avatars/322912889566396429/13c9669538850be627170ea50f152bcf.webp?size=1024" alt="">
                                </div>
                                <div class="ml-2">
                                    <div class="h5 m-0">Ride Dirt Bikes <i class="fal fa-badge-check text-primary"></i></div>
                                    <div class="h7 text-muted">@ridedirtbikes</div>
                                </div>
                            </div>
                            <div>
                                <div class="dropdown">
                                    <button class="btn btn-link text-secondary" type="button" id="gedf-drop1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="gedf-drop1">
                                        <a class="dropdown-item" href="#">Report</a>
                                        <a class="dropdown-item" href="#">Delete</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-body" style="padding-top:0px;">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/VHD4_vrVlKg" allowfullscreen></iframe>
                        </div><br>
                        <p class="card-text">
                            A new video has been uploaded go check it out!
                        </p>
                        <div class="text-muted h7" style="padding:0px;margin:0px;"> <i class="fal fa-clock"></i> October 28, 2020 at 5:46PM</div>
                    </div>
                    <div class="card-footer">
                        <a href="#" class="card-link  text-secondary"><i class="far fa-thumbs-up"></i> Like</a>
                        <a href="#" class="card-link  text-secondary"><i class="far fa-comment"></i> Comment</a>
                        <a href="#" class="card-link  text-secondary"><i class="far fa-share"></i> Share</a>
                    </div>
                </div><br><br>
                <!-- Post /////-->


                                                <!--- \\\\\\\Post-->
                                                <div class="card gedf-card" style="border-radius: 25px;">
                    <div class="card-header" style="border:none;">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="mr-2">
                                    <img class="rounded-circle" width="45" src="https://media.discordapp.net/attachments/769556481963589643/770411926764257280/SGDB.png?width=475&height=476" alt="">
                                </div>
                                <div class="ml-2">
                                    <div class="h5 m-0">Shift Gears Drink Beers <i class="fal fa-badge-check text-primary"></i></div>
                                    <div class="h7 text-muted">@shiftgearsdrinkbeers</div>
                                </div>
                            </div>
                            <div>
                                <div class="dropdown">
                                    <button class="btn btn-link text-secondary" type="button" id="gedf-drop1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="gedf-drop1">
                                        <a class="dropdown-item" href="#">Report</a>
                                        <a class="dropdown-item" href="#">Delete</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-body" style="padding-top:0px;">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/nUzz_mrz7BM" allowfullscreen></iframe>
                        </div><br>
                        <p class="card-text">
                            A new video has been uploaded go check it out!
                        </p>
                        <div class="text-muted h7" style="padding:0px;margin:0px;"> <i class="fal fa-clock"></i> October 28, 2020 at 2:51PM</div>
                    </div>
                    <div class="card-footer">
                        <a href="#" class="card-link  text-secondary"><i class="far fa-thumbs-up"></i> Like</a>
                        <a href="#" class="card-link  text-secondary"><i class="far fa-comment"></i> Comment</a>
                        <a href="#" class="card-link  text-secondary"><i class="far fa-share"></i> Share</a>
                    </div>
                </div><br><br>
                <!-- Post /////-->


                <!--- \\\\\\\Post-->
<div class="card gedf-card" style="border-radius: 25px;">
                    <div class="card-header" style="border:none;">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="mr-2">
                                    <img class="rounded-circle" width="45" src="https://media.discordapp.net/attachments/756086069275131945/770857447988264960/brown-and-white-lynx-in-close-photography-148715-scaled.jpg" alt="">
                                </div>
                                <div class="ml-2">
                                    <div class="h5 m-0">The Great Lynx</div>
                                    <div class="h7 text-muted">@thegreatlynx</div>
                                </div>
                            </div>
                            <div>
                                <div class="dropdown">
                                    <button class="btn btn-link text-secondary" type="button" id="gedf-drop1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="gedf-drop1">
                                        <a class="dropdown-item" href="#">Report</a>
                                        <a class="dropdown-item" href="#">Delete</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-body" style="padding-top:0px;">
                        <!--<a class="card-link" href="#">
                            <h5 class="card-title">Our Example Post</h5>
                        </a>-->
                        <!--<div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/sbct-GaNLlA" allowfullscreen></iframe>
                        </div><br>-->

                        <p class="text-danger" style="padding:0px;margin:0px;"><i class="far fa-exclamation-triangle" style=""></i> This post has been removed by the user or hidden.</p><br>
                        <div class="text-muted h7" style="padding:0px;margin:0px;"> <i class="fal fa-clock"></i> October 28, 2020 at 12:46AM</div>
                    </div>
                    <!--<div class="card-footer">
                        <a href="#" class="card-link  text-secondary"><i class="far fa-thumbs-up"></i> Like</a>
                        <a href="#" class="card-link  text-secondary"><i class="far fa-comment"></i> Comment</a>
                        <a href="#" class="card-link  text-secondary"><i class="far fa-share"></i> Share</a>
                    </div>-->
                </div><br><br>
                <!-- Post /////-->

<!--- \\\\\\\Post-->
<div class="card gedf-card" style="border-radius: 25px;">
                    <div class="card-header" style="border:none;">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="mr-2">
                                    <img class="rounded-circle" width="45" src="https://media.discordapp.net/attachments/756086069275131945/770857447988264960/brown-and-white-lynx-in-close-photography-148715-scaled.jpg" alt="">
                                </div>
                                <div class="ml-2">
                                    <div class="h5 m-0">The Great Lynx</div>
                                    <div class="h7 text-muted">@thegreatlynx</div>
                                </div>
                            </div>
                            <div>
                                <div class="dropdown">
                                    <button class="btn btn-link text-secondary" type="button" id="gedf-drop1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="gedf-drop1">
                                        <a class="dropdown-item" href="#">Report</a>
                                        <a class="dropdown-item" href="#">Delete</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-body" style="padding-top:0px;">
                        <!--<a class="card-link" href="#">
                            <h5 class="card-title">Our Example Post</h5>
                        </a>-->
                        <!--<div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/sbct-GaNLlA" allowfullscreen></iframe>
                        </div><br>-->

                        <img src="https://scontent-lga3-1.xx.fbcdn.net/v/t1.0-9/122998740_220225612792470_652914364225064424_n.jpg?_nc_cat=104&ccb=2&_nc_sid=730e14&_nc_ohc=EkkfuYseiNwAX9dxXty&_nc_ht=scontent-lga3-1.xx&oh=5763f7259c2215b3ac9af4b7c80f2d09&oe=5FBD36E0" class="img-fluid" alt="Responsive image"><br><br>
                        <p class="card-text">
                            Look a tree!
                        </p>
                        <div class="text-muted h7" style="padding:0px;margin:0px;"> <i class="fal fa-clock"></i> October 28, 2020 at 12:46AM</div>
                    </div>
                    <div class="card-footer">
                        <a href="#" class="card-link  text-secondary"><i class="far fa-thumbs-up"></i> 1 Like</a>
                        <a href="#" class="card-link  text-secondary"><i class="far fa-comment"></i> Comment</a>
                        <a href="#" class="card-link  text-secondary"><i class="far fa-share"></i> Share</a>
                    </div>
                </div><br><br>
                <!-- Post /////-->


                                                <!--- \\\\\\\Post-->
                                                <div class="card gedf-card" style="border-radius: 25px;">
                    <div class="card-header" style="border:none;">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="mr-2">
                                    <img class="rounded-circle" width="45" src="https://media.discordapp.net/attachments/756086069275131945/770857447988264960/brown-and-white-lynx-in-close-photography-148715-scaled.jpg?width=560&height=560" alt="">
                                </div>
                                <div class="ml-2">
                                    <div class="h5 m-0">Tek Xylo <i class="fal fa-badge-check text-primary"></i></div>
                                    <div class="h7 text-muted">@tekxylo</div>
                                </div>
                            </div>
                            <div>
                                <div class="dropdown">
                                    <button class="btn btn-link text-secondary" type="button" id="gedf-drop1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="gedf-drop1">
                                        <a class="dropdown-item" href="#">Report</a>
                                        <a class="dropdown-item" href="#">Delete</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-body" style="padding-top:0px;">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/y0LSa8ULb8s" allowfullscreen></iframe>
                        </div><br>
                        <p class="card-text">
                            This song is pretty cool you guys should go check it out! Coding without music is not possible especially like a big project like Impact
                        </p>
                        <div class="text-muted h7" style="padding:0px;margin:0px;"> <i class="fal fa-clock"></i> October 28, 2020 at 12:53AM</div>
                    </div>
                    <div class="card-footer">
                        <a href="#" class="card-link  text-secondary"><i class="far fa-thumbs-up"></i> Like</a>
                        <a href="#" class="card-link  text-secondary"><i class="far fa-comment"></i> Comment</a>
                        <a href="#" class="card-link  text-secondary"><i class="far fa-share"></i> Share</a>
                    </div>
                </div><br><br>
                <!-- Post /////-->

                <!--- \\\\\\\Post-->
                <div class="card gedf-card" style="border-radius: 25px;">
                    <div class="card-header" style="border:none;">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="mr-2">
                                    <img class="rounded-circle" width="45" src="https://cdn.discordapp.com/avatars/322912889566396429/13c9669538850be627170ea50f152bcf.webp?size=1024" alt="">
                                </div>
                                <div class="ml-2">
                                    <div class="h5 m-0">Ride Dirt Bikes <i class="fal fa-badge-check text-primary"></i></div>
                                    <div class="h7 text-muted">@ridedirtbikes</div>
                                </div>
                            </div>
                            <div>
                                <div class="dropdown">
                                    <button class="btn btn-link text-secondary" type="button" id="gedf-drop1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="gedf-drop1">
                                        <a class="dropdown-item" href="#">Report</a>
                                        <a class="dropdown-item" href="#">Delete</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-body" style="padding-top:0px;">
                        <!--<a class="card-link" href="#">
                            <h5 class="card-title">Our Example Post</h5>
                        </a>-->
                        <!--<div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/sbct-GaNLlA" allowfullscreen></iframe>
                        </div><br>-->

                        <img src="/br.png" class="img-fluid" alt="Responsive image"><br><br>
                        <p class="card-text">
                            Go check out the Episode 1 soon of the bike rebuild monday!
                        </p>
                        <div class="text-muted h7" style="padding:0px;margin:0px;"> <i class="fal fa-clock"></i> October 28, 2020 at 12:46AM</div>
                    </div>
                    <div class="card-footer">
                        <a href="#" class="card-link  text-secondary"><i class="far fa-thumbs-up"></i> 1 Like</a>
                        <a href="#" class="card-link  text-secondary"><i class="far fa-comment"></i> Comment</a>
                        <a href="#" class="card-link  text-secondary"><i class="far fa-share"></i> Share</a>
                    </div>
                </div><br><br>
                <!-- Post /////-->



                @endif

                @if($active == 'events')
                <livewire:dashboard.components.events/>
                @endif

                @if($active == 'history')
                <livewire:dashboard.components.rides/>
                @endif

                @if($active == 'vehicles')
                <livewire:dashboard.components.vehicles/>
                @endif

            </div>
            <div class="col-md-3">
            <div class="card gedf-card" style="border-radius: 25px;">
                    <div class="card-body">
                        <h5 class="card-title">Weather In Shediac, NB</h5>
                        <h6 class="card-subtitle mb-2 text-muted" style="font-size:35px;"><i class="far fa-cloud-snow text-gray"></i> -9°C</h6>
                        <p class="card-text" style="padding:0px;margin:0px;">Risks of snowfall</p>
                    </div>
                </div><br>
                <div class="card gedf-card" style="border-radius: 25px;">
                    <div class="card-body">
                    <img class="rounded-circle" width="45" src="https://cdn.discordapp.com/avatars/322912889566396429/13c9669538850be627170ea50f152bcf.webp?size=1024" alt="" style="margin-bottom:15px;">
                        <h5 class="card-title">Ride Dirt Bikes</h5>
                        <h6 class="card-subtitle mb-2 text-muted">Official Partner</h6>
                        <p class="card-text">
                        Dirt biking is a passion of mine that I take very seriously. In my videos, I describe my technique of riding trails and it also shows my course of learning how to ride motocross tracks. This channel is not only about riding dirt bikes, it is also about fixing and maintaining them. I am starting a massive bike project that will soon be published. It will have in-depth how-to videos on almost everything you could need to know on dirt bikes from the wheels to the bars.
                        </p>
                        <a href="https://www.youtube.com/channel/UC60YvZ70lwmwCJRpVLb0Vfw?sub_confirmation=1" class="card-link">Creator Channel</a>
                    </div>
                </div><br>
                <div class="card gedf-card" style="border-radius: 25px;">
                    <div class="card-body">
                    <img class="rounded-circle" width="45" src="https://media.discordapp.net/attachments/769556481963589643/770411926764257280/SGDB.png?width=475&height=476" alt="" style="margin-bottom:15px;">
                        <h5 class="card-title">Shift Gears Drink Beers</h5>
                        <h6 class="card-subtitle mb-2 text-muted">Official Partner</h6>
                        <p class="card-text">
                        Dirt bike channel focusing on having a good time in the enduro world. On my channel you'll find ride videos, reviews, and maintenance tips.
                        </p>
                        <a href="https://www.youtube.com/channel/UCAK_OcDxXlUMvgUmAA6H62w" class="card-link">Creator Channel</a>
                    </div>
                </div><br>
                <div class="card gedf-card"  style="border-radius: 25px;margin-top:10px;">
                        <div class="card-body">
                        <img class="rounded-circle" width="45" src="https://media.discordapp.net/attachments/756086069275131945/770012382549639178/119438745_3198057123642433_5296281851253655353_n.png" alt="" style="margin-bottom:15px;">
                    
                            <h5 class="card-title">G-Portal</h5>
                            <h6 class="card-subtitle mb-2 text-muted">Game Hosting Company</h6>
                            <p class="card-text">Today the #Halloween event - Fear Evolved 4 starts in #ARK. Not only is it spooky, there is also a lot of cool stuff to get your hands on, like scary dino skins and some new items; #Chibis is our new favourite! What are you looking forward to the most?</p>
                            <a href="https://g-portal.com/arksurvival" class="card-link">https://g-portal.com/arksurvival</a>
                        </div>
                    </div>
            </div>
        </div>
    </div>