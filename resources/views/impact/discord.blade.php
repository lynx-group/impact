
@extends('layouts.app')

@section('content')
<main class="page login-page">
   <section class="clean-block dark py-4" style="min-height:600px;">
      <div class="container">


         <div class="card" style="    max-width: 500px;    margin: auto;
            padding: 40px;border-radius:25px;">
            <div class="card-body">
               
            
            <center>
            <div class="user-avatar" style="display: block;margin-left: auto;margin-right: auto;background-image: url('https://media.discordapp.net/attachments/756086069275131945/769594596077862952/impact-discord.png?width=468&height=468');width:75px;height:75px;"></div>

            <br><h5 class="text-center"> Impact Offroading</h5>
            <p class="text-center">You've been invited to join the Impact Offroading Discord Server!</strong></p>

            <strong>{{$count}} Members</strong><br><br>

            <a class="btn btn-primary btn-block" href="https://discord.gg/acAM8gg">Join Server</a>
            <p style="margin-top:10px;"><i class="far fa-exclamation-circle"></i> Verification Required to Join!</p>
            </center>


            </div>
         </div>

      </div>
   </section>
</main>
@endsection