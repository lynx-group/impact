
<main class="page landing-page">

<!--<section class="clean-block clean-hero" style="background-image:url('https://i.pinimg.com/originals/df/ae/9f/dfae9fdc5659157fdcf96821fed0c277.jpg');color: rgba(0,0,0,0.69);">

<ul class="lightrope">
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
  <li></li>
</ul>-->

<section class="clean-block clean-hero" style="background-image:url(https://cdnb.artstation.com/p/assets/images/images/008/528/005/large/nina-vels-crystal-snow-desert.jpg);color: rgba(0,0,0,0.69);">
            <div class="text">
            <img src="/impact-w.svg" alt="Italian Trulli" class="align-center text-center" style="width:250px;margin-bottom:10px;">
                <p>Welcome to Impact! You're all in one offroading application to connect with other riders & professionals in offroading.<br>
               </p><a class="btn btn-primary btn-lg" style="border-radius:25px;" href="/register">Start Your Journey</a>
            </div>
        </section>
    </main>
    <div class="features-boxed" style="background-color:#17191b;color:#fff;">
        <div class="container">
            <div class="intro">
                <h2 class="text-center">Why Impact?</h2>
                <p class="text-center">Why should you use impact in your off road journey?</p>
            </div>
            <div class="row justify-content-center features">
                <div class="col-sm-6 col-md-5 col-lg-4 item">
                    <div class="box" style="background-color:#212529;border-radius:25px;"><i class="far fa-map-marker-alt text-primary" style="font-size:45px;margin-bottom:15px;"></i>
                        <h3 class="name">Explore</h3>
                        <p class="description">Explore new locations, new trails and carreers! The limit is limitless with impact.</p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-5 col-lg-4 item">
                    <div class="box" style="background-color:#212529;border-radius:25px;"><i class="far fa-book text-primary" style="font-size:45px;margin-bottom:15px;"></i>
                        <h3 class="name">Learn</h3>
                        <p class="description">Learn new skills, new guides or information about offroad vehicles with impact.</p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-5 col-lg-4 item">
                    <div class="box" style="background-color:#212529;border-radius:25px;"><i class="far fa-plug text-primary" style="font-size:45px;margin-bottom:15px;"></i>
                        <h3 class="name">Connect</h3>
                        <p class="description">Riding alone is boring, Bring Friends! With Impact you can invite members to rides.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="highlight-blue" style="background-color:#212529;">
        <div class="container">
            <div class="intro">
                <h2 class="text-center">Black Friday Grand Opening!</h2>
                <p class="text-center text-secondary">We'll be fully ready for you by black friday!<br> Watch out for new announcements.</p>
            </div>
        </div>
    </div>
    <div class="highlight-clean" style="background-color: #17191b;color: #fff;">
        <div class="container">
            <div class="intro">
                <h2 class="text-center">Our Featured Sponsors</h2>
                <p class="text-center">Yes! That's right we have sponsor! And they are awaiting you to take a visit to them!</p>
            </div>
            <div class="buttons"><a class="btn btn-primary" role="button" href="#">Featured Sponsors</a><button class="btn btn-secondary" type="button">OUR PARTNERS</button></div>
        </div>
    </div>
    <div class="highlight-clean" style="background-color: #17191b;color: #fff;background-image: url(https://images7.alphacoders.com/807/thumb-1920-807182.jpg);background-size: cover;">
        <div class="container">
            <div class="intro">
                <h2 class="text-center" style="color:#fff;">Propulzion API</h2>
                <p class="text-center" style="color:#fff;"><strong>Powering innovation on Earth and beyond for riders around the world with our infrastructures</strong></p>
            </div>
        </div>
    </div>
    <style>
    .lightrope {
  text-align: center;
  white-space: nowrap;
  overflow: hidden;
  position: absolute;
  z-index: 5;
  margin: -15px 0 0 0;
  padding: 0;
  pointer-events: none;
  width: 100%;
}
.lightrope li {
  position: relative;
  -webkit-animation-fill-mode: both;
          animation-fill-mode: both;
  -webkit-animation-iteration-count: infinite;
          animation-iteration-count: infinite;
  list-style: none;
  margin: 0;
  padding: 0;
  display: block;
  width: 12px;
  height: 28px;
  border-radius: 50%;
  margin: 20px;
  display: inline-block;
  background: #00f7a5;
  box-shadow: 0px 4.6666666667px 24px 3px #00f7a5;
  -webkit-animation-name: flash-1;
          animation-name: flash-1;
  -webkit-animation-duration: 2s;
          animation-duration: 2s;
}
.lightrope li:nth-child(2n+1) {
  background: cyan;
  box-shadow: 0px 4.6666666667px 24px 3px rgba(0, 255, 255, 0.5);
  -webkit-animation-name: flash-2;
          animation-name: flash-2;
  -webkit-animation-duration: 0.4s;
          animation-duration: 0.4s;
}
.lightrope li:nth-child(4n+2) {
  background: #f70094;
  box-shadow: 0px 4.6666666667px 24px 3px #f70094;
  -webkit-animation-name: flash-3;
          animation-name: flash-3;
  -webkit-animation-duration: 1.1s;
          animation-duration: 1.1s;
}
.lightrope li:nth-child(odd) {
  -webkit-animation-duration: 1.8s;
          animation-duration: 1.8s;
}
.lightrope li:nth-child(3n+1) {
  -webkit-animation-duration: 1.4s;
          animation-duration: 1.4s;
}
.lightrope li:before {
  content: "";
  position: absolute;
  background: #222;
  width: 10px;
  height: 9.3333333333px;
  border-radius: 3px;
  top: -4.6666666667px;
  left: 1px;
}
.lightrope li:after {
  content: "";
  top: -14px;
  left: 9px;
  position: absolute;
  width: 52px;
  height: 18.6666666667px;
  border-bottom: solid #222 2px;
  border-radius: 50%;
}
.lightrope li:last-child:after {
  content: none;
}
.lightrope li:first-child {
  margin-left: -40px;
}

@-webkit-keyframes flash-1 {
  0%, 100% {
    background: #00f7a5;
    box-shadow: 0px 4.6666666667px 24px 3px #00f7a5;
  }
  50% {
    background: rgba(0, 247, 165, 0.4);
    box-shadow: 0px 4.6666666667px 24px 3px rgba(0, 247, 165, 0.2);
  }
}

@keyframes flash-1 {
  0%, 100% {
    background: #00f7a5;
    box-shadow: 0px 4.6666666667px 24px 3px #00f7a5;
  }
  50% {
    background: rgba(0, 247, 165, 0.4);
    box-shadow: 0px 4.6666666667px 24px 3px rgba(0, 247, 165, 0.2);
  }
}
@-webkit-keyframes flash-2 {
  0%, 100% {
    background: cyan;
    box-shadow: 0px 4.6666666667px 24px 3px cyan;
  }
  50% {
    background: rgba(0, 255, 255, 0.4);
    box-shadow: 0px 4.6666666667px 24px 3px rgba(0, 255, 255, 0.2);
  }
}
@keyframes flash-2 {
  0%, 100% {
    background: cyan;
    box-shadow: 0px 4.6666666667px 24px 3px cyan;
  }
  50% {
    background: rgba(0, 255, 255, 0.4);
    box-shadow: 0px 4.6666666667px 24px 3px rgba(0, 255, 255, 0.2);
  }
}
@-webkit-keyframes flash-3 {
  0%, 100% {
    background: #f70094;
    box-shadow: 0px 4.6666666667px 24px 3px #f70094;
  }
  50% {
    background: rgba(247, 0, 148, 0.4);
    box-shadow: 0px 4.6666666667px 24px 3px rgba(247, 0, 148, 0.2);
  }
}
@keyframes flash-3 {
  0%, 100% {
    background: #f70094;
    box-shadow: 0px 4.6666666667px 24px 3px #f70094;
  }
  50% {
    background: rgba(247, 0, 148, 0.4);
    box-shadow: 0px 4.6666666667px 24px 3px rgba(247, 0, 148, 0.2);
  }
}

.winter-is-coming, .snow {
  z-index: 100;
  pointer-events: none;
}

.winter-is-coming {
  overflow: hidden;
  position: absolute;
  top: 0;
  height: 100%;
  width: 100%;
  max-width: 100%;
  position: fixed;
  z-index: 9100;
}

.snow {
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  -webkit-animation: falling linear infinite both;
          animation: falling linear infinite both;
  -webkit-transform: translate3D(0, -100%, 0);
          transform: translate3D(0, -100%, 0);
}
.snow--near {
  -webkit-animation-duration: 10s;
          animation-duration: 10s;
  background-image: url("https://dl6rt3mwcjzxg.cloudfront.net/assets/snow/snow-large-075d267ecbc42e3564c8ed43516dd557.png");
  background-size: contain;
}
.snow--near + .snow--alt {
  -webkit-animation-delay: 5s;
          animation-delay: 5s;
}
.snow--mid {
  -webkit-animation-duration: 20s;
          animation-duration: 20s;
  background-image: url("https://dl6rt3mwcjzxg.cloudfront.net/assets/snow/snow-medium-0b8a5e0732315b68e1f54185be7a1ad9.png");
  background-size: contain;
}
.snow--mid + .snow--alt {
  -webkit-animation-delay: 10s;
          animation-delay: 10s;
}
.snow--far {
  -webkit-animation-duration: 30s;
          animation-duration: 30s;
  background-image: url("https://dl6rt3mwcjzxg.cloudfront.net/assets/snow/snow-small-1ecd03b1fce08c24e064ff8c0a72c519.png");
  background-size: contain;
}
.snow--far + .snow--alt {
  -webkit-animation-delay: 15s;
          animation-delay: 15s;
}

@-webkit-keyframes falling {
  0% {
    -webkit-transform: translate3D(-7.5%, -100%, 0);
            transform: translate3D(-7.5%, -100%, 0);
  }
  100% {
    -webkit-transform: translate3D(7.5%, 100%, 0);
            transform: translate3D(7.5%, 100%, 0);
  }
}

@keyframes falling {
  0% {
    -webkit-transform: translate3D(-7.5%, -100%, 0);
            transform: translate3D(-7.5%, -100%, 0);
  }
  100% {
    -webkit-transform: translate3D(7.5%, 100%, 0);
            transform: translate3D(7.5%, 100%, 0);
  }
}
    </style>