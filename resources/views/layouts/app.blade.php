<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Impact</title>
	<link rel="icon" type="image/svg+xml" href="/favicon.svg">
        <!-- Styles -->
       
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha2/css/bootstrap.min.css" integrity="sha384-DhY6onE6f3zzKbjUPRc2hOzGAdEf4/Dz+WJwBvEYL/lkkIsI3ihufq9hk9K4lVoK" crossorigin="anonymous">
        <link rel="stylesheet" href="/css/bootstrap.css">
        <link rel="stylesheet" href="/fontawesome/css/all.css">
        <link rel="stylesheet" href="/css/features.css">
        <link rel="stylesheet" href="/css/highlight-blue.css">
        <link rel="stylesheet" href="/css/highlight-clean.css">
        <!-- Dark Theme -->
        <link rel="stylesheet" href="/css/dark-mode.css">
        <link rel="stylesheet" href="/css/navbar.css">

        <script src="https://www.google.com/recaptcha/api.js" async defer></script>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-LHG5E32EEE"></script>
        
    </head>


<body>

<div id="app">
<div class="winter-is-coming">
  
  <div class="snow snow--near"></div>
  <div class="snow snow--near snow--alt"></div>
  
  <div class="snow snow--mid"></div>
  <div class="snow snow--mid snow--alt"></div>
  
  <div class="snow snow--far"></div>
  <div class="snow snow--far snow--alt"></div>
</div>
<nav class="navbar navbar-expand-lg navbar-light bg-dark text-light fixed-top" style="background-color: #222425 !important;border-color: #222425 !important;">
	<a href="https://impactatv.com/" class="navbar-brand"><img src="/impact-w.svg" alt="Italian Trulli" style="height:40px;"></a>
   @if(Route::currentRouteName())
	<button type="button" class="navbar-toggler text-secondary" data-toggle="collapse" data-target="#navbarCollapse">
      <i class="far fa-bars"></i>
	</button>
	<!-- Collection of nav links, forms, and other content for toggling -->
	<div id="navbarCollapse" class="collapse navbar-collapse justify-content-start">
		<div class="navbar-nav">
			<a href="https://impactatv.com/home" class="nav-item nav-link test @if(Route::currentRouteName() === 'home-impact') text-primary @else text-light @endif active" data-toggle="tooltip" data-placement="bottom" title="Home"><i class="far fa-home"></i></a>
			<!--<div class="nav-item dropdown">
				<a href="#" class="nav-link text-light" data-toggle="dropdown">Services</a>
				<div class="dropdown-menu">
					<a href="#" class="dropdown-item">Web Design</a>
					<a href="#" class="dropdown-item">Web Development</a>
					<a href="#" class="dropdown-item">Graphic Design</a>
					<a href="#" class="dropdown-item">Digital Marketing</a>
				</div>
			</div>-->
			<a href="https://raptor.impactatv.com" class="nav-item test nav-link @if(Route::currentRouteName() === 'raptor') text-primary @else text-light @endif"><i class="fal fa-claw-marks"></i></a>
         <a href="https://impactatv.com/discord" class="nav-item test nav-link @if(Route::currentRouteName() === 'discord-impact') text-primary @else text-light @endif "><i class="far fa-comments-alt"></i></a>
			<a href="https://downloads.impactatv.com" class="nav-item test nav-link @if(Route::currentRouteName() === 'downloads') text-primary @else text-light @endif"><i class="far fa-cloud-download"></i></a> 
		</div>
		<form class="navbar-form form-inline">
			<div class="input-group search-box">								
				<input type="text" id="search" class="form-control" placeholder="Search">
			</div>
		</form>
		<div class="navbar-nav ml-auto">
         @guest
         <div class="nav-item dropdown">
				<a href="#" data-toggle="dropdown" class="nav-link user-action text-light"><img src="https://media.discordapp.net/attachments/756086069275131945/771115544865800242/1652.Noodles_Blue_Dark-820w-1180h2xipad.png?width=560&height=560" class="avatar" alt="Avatar"> Guest User </a>
            <div class="dropdown-menu dropdown-menu-right animate slideIn" aria-labelledby="navbarDropdown" style="min-width: 257px;">
                       <center>
                          <br>
                          <!-- https://ui-avatars.com/api/?name=Tek Xylo&color=0d6efd&background=2d2d2d -->
                          <div class="user-avatar" style="background-image: url('https://media.discordapp.net/attachments/756086069275131945/771115544865800242/1652.Noodles_Blue_Dark-820w-1180h2xipad.png?width=560&height=560');width:50px;height:50px;"></div>

                          <h4 class="text-light" style="margin-bottom:0px;padding-bottom:0px;font-size:18px;margin-top:7px;">Guest User</h4>
                          <p style="margin-bottom:0px;padding-bottom:0px;color:dark-gray;font-size:14px;" class="text-secondary">@guestuser</p>
                       </center>
                       <hr>
                       <a class="dropdown-item" href="/login"/>
                       <i class="far fa-user" aria-hidden="true"></i> Login
                       </a>
                       <a class="dropdown-item" href="/register"/>
                       <i class="far fa-pencil" aria-hidden="true"></i>  Register
                       </a>
                       <br>
                    </div>

			</div> 
         @else
         <div class="nav-item dropdown">
               <a href="#"  data-toggle="dropdown" class="nav-item test nav-link notifications text-light"><i class="far fa-bell"></i>
               <!--<span class="badge">1</span>-->
               </a>
               <div class="dropdown-menu dropdown-menu-right animate slideIn" aria-labelledby="navbarDropdown" style="min-width: 457px;">
                       <center>
                           <h3 class="text-light"> <i class="far fa-bell"></i> Notifications</h3>
                           <hr>
                       </center>
                       <p class="text-center">You have no notifications.</p>
                    </div>

			</div> 
         <div class="nav-item dropdown">
				<a href="#"  data-toggle="dropdown" class="nav-item test nav-link messages text-light"><i class="far fa-inbox"></i>
            <!--<span class="badge">10</span>-->
            </a>
            <div class="dropdown-menu dropdown-menu-right animate slideIn" aria-labelledby="navbarDropdown" style="min-width: 457px;">
                       <center>
                           <h3 class="text-light"> <i class="far fa-inbox"></i> Inbox</h3>
                           <hr>
                       </center>
                       <p class="text-center">You have nothing in your inbox.</p>
                    </div>

			</div> 
			<div class="nav-item dropdown">
            <a href="#" data-toggle="dropdown" class="nav-link user-action text-light"><img src="https://media.discordapp.net/attachments/756086069275131945/771078030339997696/brown-and-white-lynx-in-close-photography-148715-scaled.jpg" class="avatar" alt="Avatar"> 
            {{Auth::user()->name}}
            </a>
            <div class="dropdown-menu dropdown-menu-right animate slideIn" aria-labelledby="navbarDropdown" style="min-width: 257px;">
                       <center>
                          <!-- https://ui-avatars.com/api/?name=Tek Xylo&color=0d6efd&background=2d2d2d -->
                          <br>
                          <div class="user-avatar" style="background-image: url('https://media.discordapp.net/attachments/756086069275131945/771078030339997696/brown-and-white-lynx-in-close-photography-148715-scaled.jpg');width:50px;height:50px;"></div>

                          <h4 class="text-light" style="margin-bottom:0px;padding-bottom:0px;font-size:18px;margin-top:7px;">{{Auth::user()->name}}</h4>
                          <p style="margin-bottom:0px;padding-bottom:0px;color:dark-gray;font-size:14px;" class="text-secondary">{{ "@" . strtolower(str_replace(" ", "", Auth::user()->name)) }}</p>
                       </center>
                       <hr>
                       <a class="dropdown-item" href="/dashboard"/>
                       <i class="far fa-user" aria-hidden="true"></i> Profile
                       </a>
                       <a class="dropdown-item" href="/account/settings"/>
                       <i class="far fa-cog" aria-hidden="true"></i>  Account Settings
                       </a>
                       <a class="dropdown-item" href="#"
                          onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
                       <i class="far fa-sign-out-alt
                          " aria-hidden="true"></i>  {{ __('Logout') }}
                       </a><br>
                       <form id="logout-form" action="/logout" method="POST" class="d-none">
                          @csrf
                       </form>
                    </div>

			</div> 
         @endif
		</div> 
	</div>
   @else
   @endif
</nav>


<div class="panel">
<style>
.blink_me {
  animation: blinker 2s linear infinite;
}

@keyframes blinker {
  50% {
    opacity: 0.5;
  }
}
</style>
<div class="alert alert-warning text-white bg-warning shadow-none" role="alert" style="margin: 0px;border:none;border-radius:0px;background-color:#2b2f31 !important;"><p class="text-danger blink_me" style="padding:0px;margin:0px;"><i class="far fa-exclamation-triangle" style="margin-right:10px;"></i> CEO Tek Xylo, launching on our satellite called XYLO_48_gk439fk3 November 4th!  <a href="/space/infrastructure" class="xylo_countdown" style="text-decoration:none;color:#fff;"><strong id="xylo_countdown"></strong></a></p></div>

@isset($slot)
    {{ $slot }}
@endisset

@yield('content')

    <audio id="player" autoplay loop>
        <source src="/Underground.mp3" type="audio/mp3">
    </audio>

@if(Route::currentRouteName() === 'frontpage-impact')
<footer class="page-footer dark" style="margin-top:0px;padding-top:0px;">
   <div class="footer-copyright" style="margin-top:0px;">
    <p>© 2021 Copyrighted by Lynx Group <img src="/lynx.svg" alt="Italian Trulli" class="align-center text-center" style="width:25px;"></p>   
   </div>
</footer>
@endif

</div>

<script>
         var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-toggle="tooltip"]'))
         var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
         return new bootstrap.Tooltip(tooltipTriggerEl)
         })
         </script>
 <!-- Scripts -->
 @livewireScripts
 <script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha2/js/bootstrap.bundle.min.js" integrity="sha384-BOsAfwzjNJHrJ8cZidOg56tcQWfp6y72vEJ8xQ9w6Quywb24iOsW913URv1IS4GD" crossorigin="anonymous"></script>
 <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-LHG5E32EEE');

        document.body.onmousedown = function() { 
         console.log("Event.");
         document.getElementById("player").play();
         document.getElementById("player").volume = 0.2;
         }

         // Set the date we're counting down to
var countDownDate = new Date("Nov 4, 2020 15:00:00").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();
    
  // Find the distance between now and the count down date
  var distance = countDownDate - now;
    
  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
  // Output the result in an element with id="demo"
  document.getElementById("xylo_countdown").innerHTML = days + ":" + hours + ":"
  + minutes + ":" + seconds + "";
    
  // If the count down is over, write some text 
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("xylo_countdown").innerHTML = "GODSPEED XYLO!";
  }
}, 1000);
        </script>
</body>
</html>
