<main class="page login-page">
<div class="alert alert-warning text-white bg-warning shadow-none" role="alert" style="margin: 0px;border:none;border-radius:0px;background-color:#2b2f31 !important;"><p class="text-warning" style="padding:0px;margin:0px;"><i class="far fa-ghost" style="color:#fff;margin-right:10px;"></i> Happy Halloween from the Impact Team & Lynx Group! We hope you wont be tricked.</p></div>
   
    <div class="features-boxed" style="background-color:#17191b;color:#fff;">
        <div class="container">
            <div class="intro">
                <h2 class="text-center"><i class="fal fa-claw-marks"></i> Raptor Network</h2>
                <p class="text-center">Our Official Gaming Network</p>
            </div>
            <div class="row justify-content-center features">
                <div class="col-sm-6 col-md-5 col-lg-4 item">
                    <div class="box" style="background-color:#212529;border-radius:25px;">
                    <img src="https://vignette.wikia.nocookie.net/vsbattles/images/4/43/Ark_Genesis.png/revision/latest/scale-to-width-down/340?cb=20200402175315" alt="Italian Trulli" style="height:90px;margin-bottom:15px;">
                        <h3 class="name">Genesis</h3>
                        <p class="description">ARK: Survival Evolved</p>
                        <a href="" class="btn btn-primary" style="border-radius:25px;">Connect & Start Playing</a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-5 col-lg-4 item">
                    <div class="box" style="background-color:#212529;border-radius:25px;">
                    <img src="https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/0/09/ARK-_Aberration.png/revision/latest/scale-to-width-down/250?cb=20180927203421" alt="Italian Trulli" style="height:90px;margin-bottom:15px;">
                    
                        <h3 class="name">Aberration</h3>
                        <p class="description">ARK: Survival Evolved</p>
                        <a href="" class="btn btn-primary" style="border-radius:25px;">Connect & Start Playing</a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-5 col-lg-4 item">
                    <div class="box" style="background-color:#212529;border-radius:25px;">
                    <img src="https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/6/60/ARK-_Scorched_Earth.png/revision/latest/scale-to-width-down/250?cb=20180927203521" alt="Italian Trulli" style="height:90px;margin-bottom:15px;">
                    <h3 class="name">Scorched Earth</h3>
                        <p class="description">ARK: Survival Evolved</p>
                        <a href="" class="btn btn-primary" style="border-radius:25px;">Connect & Start Playing</a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-5 col-lg-4 item">
                    <div class="box" style="background-color:#212529;border-radius:25px;">
                    <img src="https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/8/80/ARK-_Crystal_Isles.png/revision/latest/scale-to-width-down/291?cb=20200524232630" alt="Italian Trulli" style="height:90px;margin-bottom:15px;">
                    <h3 class="name">Crystal Isles</h3>
                        <p class="description">ARK: Survival Evolved</p>
                        <a href="" class="btn btn-primary" style="border-radius:25px;">Connect & Start Playing</a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-5 col-lg-4 item">
                    <div class="box" style="background-color:#212529;border-radius:25px;">
                    <img src="https://static.wikia.nocookie.net/arksurvivalevolved_gamepedia/images/c/c7/ARK-_Valguero.png/revision/latest/scale-to-width-down/550?cb=20190627162429" alt="Italian Trulli" style="height:90px;margin-bottom:15px;">
                    <h3 class="name">Valguero</h3>
                        <p class="description">ARK: Survival Evolved</p>
                        <a href="" class="btn btn-primary" style="border-radius:25px;">Connect & Start Playing</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
