<main class="page login-page">

<div class="highlight-blue style=" style="
    background-image: url(https://images7.alphacoders.com/807/thumb-1920-807182.jpg);
    background-size: cover;
">
        <div class="container">
            <div class="intro">
                <h2 class="text-center">Propulzion Infrastructure</h2>
                <p class="text-center">Powering innovation on Earth and beyond with our new technologies available in Impact!<br><br>Powered by Azure Orbital</p>
            </div>
        </div> 
    </div>
   <section class="clean-block dark py-4" style="min-height:600px;">
      <div class="container">    


 
         <p style="padding:0px;margin:0px;font-size:25px;" class=""><i class="far fa-globe-americas"></i> <strong>Live Photos & Videos</strong></p>
         <p class="text-secondary">Theses images & photos are provided from satelites connected to our Ground Station.</p><hr>
         <p class="text-danger" style="padding:0px;margin:0px;"><strong><i class="far fa-times-circle"></i> No data has been received from satellites.</strong></p>
         <!--<img src="https://impactatv.com/gps-datamap/cache/U7.png" class="img-fluid" alt="Responsive image"><br>-->
         <!--<iframe class="embed-responsive-item" src="https://ustream.tv/embed/17074538" allowfullscreen=""></iframe>-->
         <br><br> 
         <p style="padding:0px;margin:0px;font-size:25px;" class=""><i class="far fa-planet-moon"></i> <strong>Space Infrastructure Status</strong></p><hr>
         <div class="card" style="margin: auto;border-radius:25px;">
            <div class="card-body">
                  
                  <div class="col-sm-12">
                  <p style="padding:0px;margin:0px;font-size:25px;" class="text-primary"><i class="far fa-satellite"></i> <strong>Satellite XYLO_48_gk439fk3</strong></p>
                  <p class="text-danger" style="padding:0px;margin:0px;"><strong><i class="far fa-times-circle"></i> Outage | Disconnected.</strong></p>
                  <p class="text-danger" style="padding:0px;margin:0px;"><strong><i class="far fa-times-circle"></i> System Not Ready</strong></p>
                  <p class="text-info" style="padding:0px;margin:0px;"><strong><i class="far fa-rocket"></i> Launching Soon</strong></p>
                  <br>
                  <p class="text-success" style="padding:0px;margin:0px;"><strong><i class="far fa-check-circle"></i> Propulzion API Operational</strong></p>
                  <p class="text-warning" style="padding:0px;margin:0px;"><strong><i class="far fa-exclamation-circle"></i> Propulzion API Latest Version v1.8 | Not Installed</strong></p>
                  <br>
                  <p class="text-secondary" style="padding:0px;margin:0px;"><strong><i class="far fa-tachometer-alt-fast"></i> Speed Unavailable</strong></p>
                  <p class="text-secondary" style="padding:0px;margin:0px;"><strong><i class="far fa-location"></i> Location Unavailable</strong></p>
                  </div>
            </div>
         </div>
         <br>
         <div class="card" style="margin: auto;border-radius:25px;">
            <div class="card-body">
               
                  <div class="col-sm-12">
                  <p style="padding:0px;margin:0px;font-size:25px;" class="text-primary"><i class="far fa-satellite"></i> <strong>Satellite 19_392dk3B (wyvern)</strong></p>
                  <p class="text-success" style="padding:0px;margin:0px;"><strong><i class="far fa-check-circle"></i> Operational | Connected.</strong></p>
                  <p class="text-warning" style="padding:0px;margin:0px;"><strong><i class="fad fa-signal-alt-2"></i> Signal 116W | Degraded Connection</strong></p>
                  <p class="text-success" style="padding:0px;margin:0px;"><strong><i class="far fa-hdd"></i> Data Collection Over-Age : 31.22GB</strong></p>
                  <p class="text-success" style="padding:0px;margin:0px;"><strong><i class="far fa-microchip"></i> Normal Load</strong></p>
                  <p class="text-success" style="padding:0px;margin:0px;"><strong><i class="far fa-check-circle"></i> System Ready</strong></p>
                  <br>
                  <p class="text-success" style="padding:0px;margin:0px;"><strong><i class="far fa-check-circle"></i> Propulzion API Operational</strong></p>
                  <p class="text-success" style="padding:0px;margin:0px;"><strong><i class="far fa-check-circle"></i> Propulzion API Latest Version v1.8 | Up to date</strong></p>
                  <br>
                  <p class="text-secondary" style="padding:0px;margin:0px;"><strong><i class="far fa-tachometer-alt-fast"></i> Average Speed Today : 7.620441km/s | 0.004666miles/s</strong></p>
                  <p class="text-secondary" style="padding:0px;margin:0px;"><strong><i class="far fa-location"></i> Location Unavailable</strong></p>
                  <p class="text-secondary" style="padding:0px;margin:0px;"><strong><i class="far fa-satellite"></i> NGSO | LEO</strong></p>
                  <p class="text-secondary" style="padding:0px;margin:0px;"><strong><i class="far fa-fire"></i> https://api.impactatv.com/propulzion?sat=19</strong></p>
                  </div>
            </div>
         </div>
         <br>
         <div class="card" style="margin: auto;border-radius:25px;">
            <div class="card-body">
               
                  <div class="col-sm-12">
                  <p style="padding:0px;margin:0px;font-size:25px;" class="text-primary"><i class="far fa-satellite"></i> <strong>Satellite 0_ISS_PANTHER (panther)</strong></p>
                  <p class="text-success" style="padding:0px;margin:0px;"><strong><i class="far fa-check-circle"></i> Operational | Connected.</strong></p>
                  <p class="text-success" style="padding:0px;margin:0px;"><strong><i class="fad fa-signal-alt"></i> Signal 341W | Excellent Connection</strong></p>
                  <p class="text-success" style="padding:0px;margin:0px;"><strong><i class="far fa-hdd"></i> Data Collection Over-Age : 0.00GB</strong></p>
                  <p class="text-info" style="padding:0px;margin:0px;"><strong><i class="far fa-microchip"></i> Low Load</strong></p>
                  <p class="text-success" style="padding:0px;margin:0px;"><strong><i class="far fa-check-circle"></i> System Ready</strong></p>
                  <br>
                  <p class="text-success" style="padding:0px;margin:0px;"><strong><i class="far fa-check-circle"></i> Propulzion API Operational</strong></p>
                  <p class="text-success" style="padding:0px;margin:0px;"><strong><i class="far fa-check-circle"></i> Propulzion API Latest Version v1.8 | Up to date</strong></p>
                  <br>
                  <p class="text-secondary" style="padding:0px;margin:0px;"><strong><i class="far fa-tachometer-alt-fast"></i> Average Speed Today : 7.510272km/s | 0.004666miles/s</strong></p>
                  <p class="text-secondary" style="padding:0px;margin:0px;"><strong><i class="far fa-location"></i> Location Disabled</strong></p>
                  <p class="text-secondary" style="padding:0px;margin:0px;"><strong><i class="far fa-satellite"></i> LEO</strong></p>
                  <p class="text-secondary" style="padding:0px;margin:0px;"><strong><i class="far fa-fire"></i> https://api.impactatv.com/propulzion?sat=0</strong></p>
                  </div>
            </div>
         </div>

        <br><div class="card" style="margin: auto;border-radius:25px;">
            <div class="card-body">
               
                  <div class="col-sm-12">
                  <p style="padding:0px;margin:0px;font-size:25px;" class="text-primary"><i class="far fa-satellite"></i> <strong>Satellite 53_dd99191A</strong></p>
                  <p class="text-success" style="padding:0px;margin:0px;"><strong><i class="far fa-check-circle"></i> Operational | Connected.</strong></p>
                  <p class="text-success" style="padding:0px;margin:0px;"><strong><i class="fad fa-signal-alt"></i> Signal 312W | Excellent Connection</strong></p>
                  <p class="text-danger" style="padding:0px;margin:0px;"><strong><i class="far fa-times-circle"></i> System Not Ready</strong></p>
                  <br>
                  <p class="text-success" style="padding:0px;margin:0px;"><strong><i class="far fa-check-circle"></i> Propulzion API Operational</strong></p>
                  <p class="text-warning" style="padding:0px;margin:0px;"><strong><i class="far fa-exclamation-circle"></i> Propulzion API Latest Version v1.8 | Not Installed</strong></p>
                  <br>
                  <p class="text-secondary" style="padding:0px;margin:0px;"><strong><i class="far fa-tachometer-alt-fast"></i> Speed Unavailable</strong></p>
                  <p class="text-secondary" style="padding:0px;margin:0px;"><strong><i class="far fa-location"></i> Location Unavailable</strong></p>
                  <p class="text-secondary" style="padding:0px;margin:0px;"><strong><i class="far fa-satellite"></i> NGSO | MEO</strong></p>
                  <p class="text-secondary" style="padding:0px;margin:0px;"><strong><i class="far fa-fire"></i> https://api.impactatv.com/propulzion?sat=53</strong></p>
                  
                  </div>
            </div>
         </div>

         <br><div class="card" style="margin: auto;border-radius:25px;">
            <div class="card-body">
               
                  <div class="col-sm-12">
                  <p style="padding:0px;margin:0px;font-size:25px;" class="text-primary"><i class="far fa-satellite"></i> <strong>Satellite 11_ab25715W</strong></p>
                  <p class="text-danger" style="padding:0px;margin:0px;"><strong><i class="far fa-times-circle"></i> Outage | Disconnected.</strong></p>
                  <p class="text-danger" style="padding:0px;margin:0px;"><strong><i class="far fa-times-circle"></i> System Not Ready</strong></p>
                  <br>
                  <p class="text-success" style="padding:0px;margin:0px;"><strong><i class="far fa-check-circle"></i> Propulzion API Operational</strong></p>
                  <p class="text-warning" style="padding:0px;margin:0px;"><strong><i class="far fa-exclamation-circle"></i> Propulzion API Latest Version v1.8 | Not Installed</strong></p>
                  <br>
                  <p class="text-secondary" style="padding:0px;margin:0px;"><strong><i class="far fa-tachometer-alt-fast"></i> Speed Unavailable</strong></p>
                  <p class="text-secondary" style="padding:0px;margin:0px;"><strong><i class="far fa-location"></i> Location Unavailable</strong></p>
                  </div>
            </div>
         </div>

         <br><div class="card" style="margin: auto;border-radius:25px;">
            <div class="card-body">
               
                  <div class="col-sm-12">
                  <p style="padding:0px;margin:0px;font-size:25px;" class="text-primary"><i class="far fa-satellite-dish"></i> <strong>Impact Orbit Station by Azure</strong></p>
                  <p class="text-danger" style="padding:0px;margin:0px;"><strong><i class="far fa-times-circle"></i> Outage | Disconnected.</strong></p>
                  <p class="text-danger" style="padding:0px;margin:0px;"><strong><i class="far fa-times-circle"></i> System Not Ready</strong></p>
                  <br>
                  <p class="text-secondary" style="padding:0px;margin:0px;"><strong><i class="far fa-microchip"></i> Unavailable</strong></p>
                  </div>
            </div>
         </div>

      </div>
   </section>
</main>
