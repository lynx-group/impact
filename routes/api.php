<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::domain('api.' . env('APP_URL'))->group(function ()
{
    Route::middleware('api')->get('/stats', function()
    {
    $obj = (object) [
        'user_count' => 0,
        'pingstatus' => 2000
    ];
    

    return json_encode($obj);
    });


    Route::middleware('api')->get('/propulzion', function(Request $request)
    {


        if($request->input('sat') == '19') {
            $obj = (object) [
                'name' => "wyvern",
                'unique_name' => "Satellite 19_392dk3B",
                "sat_speed" => 7.510272,
                "sat_type" => "NGSO GEO",
                "sat_repsonse_code" => 1,
                "propulzion" => [
                    "version" => 1.8,
                    "update_available" => false,
                    "installed" => true,
                    "operational" => true,
                ],
                "latest_data" => [
                    "pz_version" => 1.8,
                    "date" => "10-30-2020",
                    "smp" => "https://impactatv.com/gps-datamap/cache/F8.png",
                    "smp" => [
			"latest" => "https://impactatv.com/gps-datamap/cache/latest30.png",
                        "F8" => "https://impactatv.com/gps-datamap/cache/F8.png",
                        "X5" => "https://impactatv.com/gps-datamap/cache/X5.png",
                    ],
                    "geo-data" => "33 cc dc b2 ee 48 30 5a 8e 15 a5 84 16 4a d6 76 
                    c0 b9 e1 72 d9 f0 e2 70 8e 84 0b 71 e4 10 e8 3c 
                    05 05 59 bc 0e 3a 81 b7 4e d8 ba e4 dc a4 00 5a 
                    67 0e 4c 37 f5 56 e5 34 d7 e0 a4 49 83 9c 97 32 
                    b6 63 04 e7 79 55 7c 44 15 25 f9 67 56 c2 25 42 
                    3c e9 5a 22 4c 39 e6 f2 65 3a 71 ac 37 a3 43 f2 
                    32 4a b4 7a 81 4f 28 6a fb 31 66 0a ac ff 7a 81 
                    4a 9c 8b 75 7e bb aa 81 b4 e3 81 e5 40 01 19 fa 
                    b7 f0 45 27 d6 2b c7 12 c3 00 bb 0d da c1 07 bb 
                    48 64 0f bb 8e 10 96 29 69 0d ca 4a b7 c1 c6 c1 
                    c8 29 2d 22 1b 4d c9 62 0a 3d 67 7b 6f 33 e7 e8 
                    a3 f5 df 1e 70 e5 b5 38 4d 7d 41 f9 fd 40 a8 46 
                    eb eb 65 15 01 52 02 f5 15 06 93 82 e2 c3 2e 5a 
                    65 0f 78 0c 31 3a 44 2d fd b4 ad 9c e5 b7 1f 35 
                    27 c2 6c ba 9d 85 8d 01 b0 6f 55 86 5b 1b dd 14 
                    51 f7 b4 5e 9d 6b 7a 4c e5 73 4c 55 84 6e 0d 88 
                    6d ed e8 41 cf 9f fa fb ce 35 88 e6 e1 2a b8 30 
                    01 5a aa 62 12 88 9d 3b 36 0b 83 12 16 39 29 8e 
                    94 9a a9 ea c4 dd 3e 25 cc fe 36 5f e1 58 e1 d3 
                    22 57 cd d7 a1 ac 1b f3 10 81 06 25 1b 58 39 15 
                    84 e0 1d cf 3f 6c 28 02 2a 56 88 6e b2 14 a3 51 
                    cb 6e 34 44 81 d7 f5 ab 17 b1 5b a2 76 98 20 2c 
                    dc 2e 0e 0e 14 aa c9 1d b5 78 cc 51 f8 92 37 40 
                    27 71 d4 80 b9 d8 ca 5f 19 f1 26 33 60 56 94 89 
                    b7 6a 8a 45 31 5b ae 4a bb 9b 90 9c 09 dc 29 e6 
                    c9 50 c8 f1 93 aa ee 29 bb dd 8a 68 d6 7a 83 6c 
                    7f 7a b4 81 8b 42 78 48 4a c9 97 77 0a 71 86 fa 
                    02 a8 2a 8a f4 e2 01 5d 86 30 81 67 a3 b2 78 ae 
                    33 f2 82 0b 99 2c e5 ef 47 8e 52 d8 99 97 95 9f 
                    a6 c4 f4 14 29 17 70 69 85 fb fd 6c 99 52 c6 30 
                    02 a0 46 4a 9d d0 d1 69 2d 57 1e 65 f0 d2 58 7e 
                    4d af 33 8b 35 b8 bb 7a 4a f3 ac 9e 52 49 02 7c 
                    ca 8c c7 01 09 2b 72 b9 ca e9 a8 ee 42 50 9d c5 
                    cf fa bd f0 52 37 1d 73 23 51 88 63 a8 c4 2e 18 
                    18 3b ed 71 e6 67 a0 99 66 a6 c9 02 ae bb de e8 
                    ce 1f 29 17 c8 74 e3 f6 28 eb 15 f6 74 a7 42 e7 
                    63 c4 4e 7b 0d 31 62 34 5d b8 2c 64 27 0c 7c 79 
                    1a 58 91 9f 8c 98 17 7f 69 98 33 48 12 9b b7 52 
                    8f 7a dc d6 c8 c3 aa c6 f9 2c d9 f8 45 f1 6f ef 
                    2b ce da 49 03 86 02 41 51 6e 2a 11 d5 90 8e 1c 
                    75 c6 ab 5a ea 98 8a bb dc d7 0c c1 b5 95 b6 d0 
                    90 63 c8 14 69 84 ba 16 20 63 65 46 7a 10 57 03 
                    4f b4 20 99 ab 9b 00 d4 ba b9 2a ae 04 01 7c e6 
                    f3 3a da 99 3d ce 9f 1a e4 ab 60 a0 0a d3 3b b7 
                    28 17 59 59 12 27 0a 71 82 c5 9f 6a ba fe 3a 12 
                    19 ca 3d f3 fa 49 f1 5f 99 cf a6 87 c7 29 6d cc 
                    42 04 59 08 e0 13 f7 7a 3f 6e 71 a8 84 88 68 50 
                    dd ef fa c4 7a ed 20 72 45 88 9e bc ea 3a 38 31 
                    76 65 71 ba 72 84 c7 9e 45 e4 57 b3 f4 79 a3 08 
                    45 79 d8 3b 98 53 92 46 71 3c 4c bc ff ac 6d 54 
                    82 97 5e bd 04 d7 a2 e5 20 0b b3 ac 06 23 17 1c 
                    d8 83 83 23 df 2e 73 34 a7 d8 17 8d 6f 9f 2c c7 
                    3b 82 56 76 77 54 e2 55 7d fa 41 ce 6f c6 6b 73 
                    76 af bd b3 5b 5e 0f 0e 96 db f7 e5 ee ab 7d fd 
                    4a 5d e0 07 38 f0 07 55 c9 9a 50 59 34 18 23 c0 
                    cf aa 64 54 0b f0 b9 34 00 b5 5b 57 29 0c 10 ec 
                    3e 14 c1 c4 ff 00 61 ac 02 81 d4 37 3c 3b 88 d7 
                    d3 8c d5 e2 ed cd 25 49 0d 87 1e 54 a7 db 62 e7 
                    df 1e fb 6f 70 82 a2 e5 55 ff 84 c0 24 6c e9 f1 
                    a9 6b 1b 06 8c f0 01 57 b0 92 12 c1 30 1d c0 45 
                    79 a6 49 3b fa 81 a1 24 6a 47 2c ae 6c 88 49 02 
                    1c 9a e5 72 b4 13 9a 5f b5 e2 1a b0 78 8e 4e 93 
                    b9 d5 d2 2f 5f 8f ec 96 7c bb 39 32 5b 3c 78 17 
                    76 79 02 38 f5 db 53 a2 f0 b5 57 98 c8 9c cd 90 
                    d3 09 c2 11 c5 cc 7d a3 f0 f8 a4 37 85 18 51 0b 
                    5a ad ec 86 70 4a 70 01 3d fe 51 fd 11 fc 60 51 
                    90 9e 34 75 eb ba bc 54 f6 cc cd 45 9c 7c 02 31 
                    ec 04 0c d4 f6 56 3f 7b bb 51 46 a0 d4 93 e0 5b 
                    fb 6d ea 16 35 1a 4b 85 ef dd 3e 8f 50 4f e8 d0 
                    96 83 e0 32 8f c3 57 7d 6e 5a 93 9a f7 8a ca d6 
                    e6 3f 23 15 33 51 b2 1a 4c c9 79 90 69 b9 1d d3 
                    2b b0 f6 0d f7 be 56 9a fe 19 d0 fe c7 fa 0a cb 
                    18 c1 c1 a9 34 16 cd 08 3d 0c c4 88 cd ce 3c c7 
                    d9 bf 03 b3 15 db 2b 78 7d 15 4e 8f 10 b9 b3 7b 
                    a6 1d fe e2 98 29 94 35 a2 ef 1b 03 82 25 bb cc 
                    1f 9b 45 d8 9d 8f da 2d 8a e5 29 38 22 2f c5 28 
                    bc 77 39 43 fb 23 10 0c da c6 b7 ca 4b da 00 57 
                    82 e8 26 ed f7 d6 fe 25 0c 8d b7 79 c9 8b f4 d7 
                    7e 27 7d 42 b5 96 a1 aa c9 d2 ae af 42 33 76 9e 
                    3a 2f 83 d0 d9 c6 d1 36 a3 d1 34 ec ef 1e da 2f 
                    0c 85 e5 17 67 d4 cf 53 a6 40 34 b2 95 94 aa 93 
                    f9 0d 2d d6 1a 38 d6 12 a5 32 f3 b0 9e c0 f2 5d 
                    24 ce bd e1 30 a3 0c 34 68 7b a8 72 99 59 ce e3 
                    71 ea cc 7a a5 db 58 19 64 8f 08 03 36 5b e5 74 
                    ba 11 d4 a3 13 9d 52 3c 8b ea b4 89 54 2a 59 5a 
                    7f 05 b8 41 e7 ef 66 86 59 de 4f 56 2f 8c de 7a 
                    6e f7 0a ba 38 40 e1 ab 6a 09 77 08 c6 7b a4 7b 
                    99 2c 45 c1 fe 96 88 e9 4c 1b 76 72 06 85 93 a0 
                    74 b8 f7 c3 8d a3 c5 a5 ea ff 20 ef f8 c6 bd ef 
                    98 b2 05 b0 a7 49 eb 2c 12 19 1c 24 96 64 24 a0 
                    50 38 57 c8 2e 42 cf b2 e5 e3 a9 dc 9d 17 57 9e 
                    28 c8 fa 00 c7 0f 0f be 75 c4 43 ee 56 b2 6a f6 
                    19 9d 11 29 86 86 24 51 04 8e 59 39 44 07 eb 21 
                    ca 97 18 a5 af e8 ac e5 db 78 c0 df 6c 2f f5 11 
                    80 99 03 e4 9e 70 be cd 57 2a 18 60 cb af 8f f3 
                    2c 52 0c 00 6e fa d2 27 23 f3 78 8b bc d2 46 b9 
                    34 92 29 97 84 87 a3 6c e4 e4 75 43 f0 39 76 e7 
                    4d 3c 6c 89 10 72 c7 68 4d a7 bb a1 a0 29 91 f0 
                    27 d9 a0 06 0f 82 3e 43 af 6f 00 58 86 ae 83 3e 
                    76 b1 93 f8 a8 17 2d 84 82 a4 b3 39 63 40 41 68 
                    cc 0a 2b e1 ee da 59 36 4f d2 fa ac a8 7c a6 c5 
                    3e 18 3b f9 3a 19 23 52 99 cb 2b b0 f4 57 f2 b0 
                    f7 ac 1d 44 ee 9f b1 33 98 2a 7f 5a f4 e4 60 3a 
                    99 a3 58 5d 85 b8 8a 77 67 cc 9b 59 24 3f ca 12 
                    2d 83 92 ef 9b e7 26 8b 22 f2 30 79 7c 5c a2 40 
                    e5 30 2b 5c ff 3c 38 26 40 f9 e0 53 41 6e 96 f8 
                    2e 5b ea 87 08 43 48 4b 68 b6 41 fd 4f ec 39 4c 
                    6e 81 a6 55 95 a1 ab fb 1f e7 cd be ec d2 65 62 
                    46 19 c0 0e 0f 64 5c 51 6f 3a c6 9f f7 b5 48 3b 
                    6a a2 7c 4e 2b 0d 7b bc ab 56 33 e8 9c 4c 95 10 
                    bb d6 16 93 18 ca 74 ec 0e a3 55 fb d8 1f c6 67 
                    e1 80 cf f6 89 d4 29 30 58 44 56 f8 c2 85 a0 21 
                    a1 ec 86 58 5c 6e aa fe 92 d1 69 7d 43 e6 b5 fb 
                    ce 44 45 f7 71 d8 61 d8 f4 a2 da 98 49 d9 ef 00 
                    7b 34 0e 2c a4 65 5d f9 75 85 1b f4 69 08 ac 14 
                    b3 e3 f9 5c 36 c2 33 2b 2b ae bd 9c c2 a6 42 70 
                    c1 2b 9d 43 ed 6b 38 61 a6 d1 3c bf fd e5 0b d2 
                    9b 74 6a 3b dc 09 b9 a5 cd 10 0b 88 2f 38 5a c3 
                    f0 1c a8 1d ea 78 1e 8a bd 9e cc 4b 3b b1 fe 31 
                    a4 2f ff d2 90 65 c5 ff b8 f7 7b 78 03 3c 20 d5 
                    5d d0 ee f8 7a 7f 39 6b 29 75 79 db 2d 6d 2b b9 
                    b8 c6 1c dc 32 3d f0 74 78 26 37 34 8c 38 3c 9e 
                    44 0d b2 f8 db cc 22 bc b0 ef 84 cb 6e ce fd c3 
                    ce fa c6 21 33 44 1a c5 19 02 01 cb 94 0e e9 89 
                    ee 85 5d 1d 2f 4e f2 33 b1 fb cd 5c b8 48 4e 97 
                    f5 60 69 2b 5e db 90 99 18 0c db f8 0d 00 6c 2e 
                    5e d1 a7 1c ee 8a 9d 6e 5e e6 ac 01 7c ec 9e 15 
                    55 45 94 37 6e 7a 24 6a 62 23 62 57 e2 2c 61 05 
                    5e e5 00 dc 08 e8 92 09 db a2 80 2a 10 1c cd 64 
                    b8 9c 02 49 77 e1 ec ee df 50 2a 07 7f e9 14 cb 
                    59 2d 3b 23 2e 53 d1 46 b2 dd ae e9 c2 00 50 2d 
                    2e 34 5e 73 d0 98 e5 5d 07 61 e5 39 97 69 ea ba 
                    3e ad eb a8 30 29 ed 27 f4 09 52 6b f4 5b 68 9e 
                    29 0c 1b 0f 85 12 a9 76 c3 54 fa f9 6c 20 f2 ce 
                    ed 3f ac 4c d9 06 f3 10 71 7e 7c 93 32 0c ef 3b 
                    d9 13 a7 40 b2 4b 8c 75 4b c0 4a c4 ec 7c a3 64 
                    d8 cc 91 76 2f 3e 02 a9 92 af 68 56 98 10 93 f2 
                    dc 39 ba 13 13 94 3d 27 a4 16 ba c6 6e 41 2c ec 
                    3b 73 99 30 1d 26 15 4f 21 f3 7a d6 bc cd 35 46 
                    d3 20 68 7a c2 4d 41 fa 6d b2 a6 4f ca dd f2 c6 
                    20 5a 31 f8 7d 1d 17 c4 81 dc de aa 8a ad bc b0 
                    f9 8b 7d 89 f9 c7 90 41 e8 d7 89 dd 13 9e 5a 6f 
                    6c 43 3b 32 96 04 e5 83 79 4e 02 ef e4 7d 99 93 
                    15 ad b6 e5 62 ef 31 67 31 26 ec cf 92 47 ae 15 
                    6a 17 de bc e5 b1 0f 91 3b 53 ef f9 6b 3c ed be 
                    32 3f 69 cf 5b f9 8f 23 58 67 34 3c c4 9a 16 40 
                    42 4c 99 ea 18 bf 05 24 66 08 6f 87 65 45 78 62 
                    ba f9 52 64 18 00 26 55 f2 dc 8c c0 78 7f b3 41 
                    f3 b1 f2 c3 d5 f4 ba 3c 5d 6f c9 1b 9b b8 1b 4f 
                    4c d0 42 8a e5 8e 9d 27 02 c6 bf d7 da b8 d8 82 
                    2f 7d 75 a3 7e a6 f2 1f 07 f0 b1 e0 03 f2 19 e2 
                    d4 22 1f 2f 40 07 84 1f 16 66 8b 03 51 16 c1 1a 
                    1b 3c 18 da 36 bd a8 c5 aa fc 9e f3 d1 01 f8 bd 
                    4c 5b cd 23 f5 c6 41 03 c3 86 54 4a 6a 70 38 47 
                    b7 f8 50 06 57 f3 92 41 00 3a da b0 3b 02 0b 71 
                    75 30 5e ba 38 1f 07 69 7d 40 b7 e7 12 8c 89 58 
                    1c 40 bb 64 58 fe 08 e5 11 69 c6 4d 87 da 06 87 
                    c6 5d fd ed b8 4d a0 86 2c db a1 34 ab 0a 5c 5f 
                    45 91 cd e9 6e 47 a8 34 7f b8 e4 6f 9d 73 69 ca 
                    33 b4 6a 19 88 84 82 43 64 8b fa 52 c4 72 57 46 
                    2d 41 cd 18 33 64 dc 10 e3 3d e2 8a a7 48 8a 9f 
                    d7 50 57 2c 9b 35 8a e6 6d d7 b3 2f 3b 40 ec 60 
                    24 b7 a6 a1 f5 93 49 47 5f a3 a9 52 80 83 74 23 
                    2f 00 c1 8a 9d b7 8e dc d1 fe 51 bf 0d 3f 60 64 
                    f0 7a d7 bb ac 95 08 2b 3d 39 71 a5 bd 5e 17 1b 
                    c4 69 2d 98 67 38 a2 43 cb 41 95 69 82 11 94 09 
                    5a 01 7c de 79 66 ed 1a 84 3c 96 13 73 27 b8 6d 
                    01 73 ad 58 fd 10 1e 38 7d 13 a0 9c d6 56 09 b7 
                    4e 3a 96 fb e4 80 4c a5 f7 4a 40 ca 4e 55 94 f3 
                    5b 05 81 d4 c1 10 cb 4a 96 a3 25 6d 8d b1 be 6f 
                    db b0 b2 3b cd 29 39 9d 71 d0 6c 78 e2 49 8a 81 
                    eb c8 87 80 67 5e f6 6f 6d 36 2b fc 57 52 e0 69 
                    3e 65 79 28 75 fc 76 52 24 3b 83 3d 08 8d 97 da 
                    1e 9c 9e 9e 9e 80 10 b1 e7 bc 6a d2 31 76 1e 03 
                    31 eb 88 d5 18 49 97 2b 8f 79 17 25 3f 46 49 83 
                    9b a7 9a f0 e4 12 7e a7 eb 46 a4 46 3c dc 4c 75 
                    b5 fd 71 89 31 22 15 59 eb 9f 21 ff 1b 5d 20 2f 
                    85 7b 79 a0 d9 29 c4 32 76 9c 20 a4 41 97 10 8c 
                    51 ae a7 e5 83 78 50 54 97 f0 02 0c 53 3b bf 3d 
                    53 de 0f a1 23 cc 77 2f fd 54 5f fd c6 40 85 b2 
                    b2 e7 1a fc 78 37 e6 89 ee 5b a5 67 d5 2b aa 03 
                    61 65 7d 47 1e 7e b1 71 7c ff 40 79 23 1a e3 89 
                    b9 b2 81 6f 27 22 0b 58 9d 99 79 43 e2 02 39 ed 
                    83 35 47 58 b5 8a 50 ea 78 17 f3 7d 3d 08 72 91 
                    c4 55 50 70 80 c1 bd a2 fe 67 de ec d5 06 ef c8 
                    27 2e ba 54 9c 2e 98 c3 33 8e 87 8b 8b 2d 4b e5 
                    2a b9 9f c8 5d 6e 50 55 4b 03 71 8e 7f b3 2a 2c 
                    e8 d1 9a 6d a5 65 a9 dc dd a9 26 25 44 cf 7b 15 
                    15 31 80 cb a9 51 8a 7c 9e ec a3 d8 37 6f b7 6f 
                    f6 d5 8a 7b dc 9c 0f a1 35 25 36 5a d6 54 49 9f 
                    3d f3 4e e8 ab ec 64 26 7a f9 d4 b6 1e bc 1e 2d 
                    d7 f5 1e 44 c9 7c 41 28 c2 36 97 67 87 ab 7c 8a 
                    b3 15 8f 68 5d fd 7b ff d3 ad e6 60 ef ae 17 15 
                    14 04 96 81 49 ed 29 f1 58 11 48 5e 8a c9 7f 20 
                    cb 5e 7a c9 04 89 68 a7 4a 1c ab a0 74 42 3b c8 
                    28 1b 28 e6 a1 33 9b 49 f0 e1 74 aa c0 1b af e7 
                    5c 08 41 7e d5 48 b4 6a 6f 3f d0 8a 75 47 c9 15 
                    b1 90 f8 9c ab 7b 91 f4 30 64 c8 07 56 9e 49 ed 
                    e6 68 d6 79 5f 4a 29 30 7b da 03 f9 c0 55 b8 a6 
                    1f 85 c5 91 47 b7 c1 36 e6 0c f4 f9 3d d9 d6 8d 
                    fe c8 b3 fa 83 bb 20 2d a6 c4 fa 43 72 1c 09 f6 
                    f4 f3 fe 31 6c cc 39 d4 0f 5e a4 85 1c 8c 93 06 
                    ff 60 52 8d 52 9f ef b8 d9 d6 8d b9 be 53 9f a0 
                    52 6f 1a 94 41 1c 12 9e 1b 14 4e c3 58 03 ce b7 
                    ed b0 8f 38 12 0a 65 9f 70 dd 9e cd 0e 1a 0e 15 
                    46 2d 14 4f 0d de 2a bc bd 69 dd 27 32 43 c7 6f 
                    19 ed e3 9d e6 18 1f d4 31 19 61 24 b4 39 98 49 
                    08 a4 0c 88 cf da 8c af 3d dd be 20 bf ce 2e f1 
                    a0 a8 81 4b a4 a2 60 0a 00 c3 2e 46 ef b8 93 fa 
                    62 05 4b 5e fd 08 53 40 a2 c3 e6 ee 01 bd 15 fb 
                    c0 52 3f c0 36 a1 b1 36 ae 25 3a 9a 54 60 14 34 
                    ae 09 d4 74 dd 43 f5 f6 c0 ed fc 4d ce 94 60 0f 
                    c1 40 26 ea bc d8 4a d3 d2 32 a2 fb 02 5a 32 2e 
                    d1 59 4e bb d5 e8 c0 5a 3d d1 f1 c4 b2 89 a0 a1 
                    95 42 72 45 f2 6e 62 0a 4d 97 8a 66 7c 59 ba 76 
                    e3 ad 8a a3 f9 fe d2 29 8d d9 52 c1 97 5c 43 6a 
                    d9 7c 0d 00 eb 79 5a 9d 17 21 3d d8 4e ec 96 0c 
                    7c 72 49 29 e8 dd a0 26 98 07 04 e2 c9 57 d1 d7 
                    f4 e9 14 d0 e7 de 03 f3 5d 27 ad 0b f7 5b 7d 40 
                    ae 60 b2 90 ca 16 a8 60 b1 24 20 77 c0 25 30 d0 
                    ed a4 ad aa 0b 02 b8 21 9a 6f 24 b5 1c 03 1c 3a 
                    37 23 2c b9 a1 b9 f5 54 60 85 42 56 9f 75 82 61 
                    d1 ce 5c de 99 76 b3 e1 bf be d2 7a 44 fa cb 7b 
                    45 81 8e 16 94 37 7d fb 45 9b 94 d8 cf 2d 6a 50 
                    5e f7 fc 96 e2 09 ee 3f 4d 3b ab 6a 16 bb f7 7d 
                    92 24 6f c5 87 52 cb a0 db 0f 1a 0c 1f 29 76 33 
                    19 cb b9 fc 94 c8 da 1a a6 6f 5e db e2 c8 53 58 
                    17 92 ae 82 bc b5 e4 35 69 6a 7f 3b c8 d2 b5 2f 
                    db 66 c1 34 dc d2 6d 2e 2d 3e ff 9b 42 b2 2a 9d 
                    75 7c de f9 b1 4b 16 73 5f f1 1d 18 cc 65 67 9d 
                    22 8e 1c 00 6c fc bb ae 75 82 ed a6 87 ab 2b 23 
                    ec 81 f5 c9 70 3d 73 b1 a6 86 24 be 4d fb 0d 2a 
                    54 05 a6 ad c6 cb 13 21 fb 2b c4 3e cd c4 77 61 
                    e3 61 8d 73 35 39 4c 9a 25 1d 5a c0 14 bb e6 80 
                    7d 82 6d 5a 44 70 15 19 cf e8 8d 54 58 6d 96 e3 
                    cb 06 da 71 dd 45 cc da 9e 16 49 0b 22 38 25 f4 
                    fb fb 66 2b 5c cc ee e1 df 67 19 62 5d eb ab bf 
                    6d 24 28 44 61 c2 26 b4 fc 31 4a 4e a1 19 7e 6f 
                    71 b3 e6 ff c9 5e a9 70 d0 7b 81 9f 4a ae 03 79 
                    6d 09 08 65 73 75 8f 8b d1 37 fa 9b 92 47 86 61 
                    a7 c7 f6 50 86 e1 55 63 8a bb 18 9d 56 e7 4d fd 
                    ad 3a b1 49 5f ac 5b da 37 0c ae 5a 71 8b d4 78 
                    04 3a d5 0a 40 81 6f d1 67 3e 6e 4b 53 bd b3 fd 
                    43 1f c7 69 b9 3b 0e 20 b1 96 9f 9d 00 81 ef 20 
                    6b e2 16 ea ba 91 cd f7 01 89 d9 19 ed 78 12 74 
                    c5 95 80 64 a7 a0 33 91 11 cd 36 8b 03 7e 1c b2 
                    24 e7 17 d4 8e 3c 67 3a 26 16 39 42 94 43 cf dd 
                    9f a7 2b cb b1 c8 a8 d2 56 60 ee 0c 33 42 b1 b4 
                    d0 88 98 95 b5 97 50 fe f0 1e d0 7c cc b2 22 dd 
                    b9 3f 7b 20 70 d2 e4 bd 80 2d 68 58 5e f3 64 ed 
                    a1 08 5b 73 33 02 79 c9 93 79 0a 4b 1d 9e 4f 7b 
                    f1 cb 72 bb 92 b6 1c 98 67 b1 e5 17 83 27 df ac 
                    19 d0 2e 42 54 b9 fd 50 6d ac 5a d3 f2 73 80 5b 
                    67 81 67 cc 60 49 3e af 5b 9e 41 f1 00 bf 95 19 
                    5f 19 5e 2e 2a b0 cb de 34 c1 30 8e 2b 0b d6 21 
                    74 60 12 57 69 56 bf 98 be 8a 06 fb f0 7e b4 a3 
                    1c 8e dc 24 77 fc f8 36 c0 8d 2d 0f dc 5b 34 67 
                    70 6d 22 22 05 94 fc e3 03 86 8a 1e 3c 04 1d ff 
                    9b b4 2e 39 33 4d 4c f2 80 c3 77 e5 08 55 22 ef 
                    49 8f 49 9b f8 9b 3b c8 1f 08 27 3f ec 87 c0 12 
                    4b cd c1 1d 4d 1d 09 16 2e 86 19 96 2c 84 36 17 
                    b1 62 83 57 ac 92 58 14 84 ac 5c 8f 93 86 51 7a 
                    00 32 b7 a8 e2 a5 8a 44 97 b0 3d 66 ef e6 8b 5b 
                    13 8b a5 5f 69 8f ff b1 be b6 e8 26 d6 30 77 77 
                    88 5f 6f 53 01 95 98 7c b2 64 87 92 f3 f0 bb f4 
                    a7 9e fe fb 6d 38 0f fb e5 8f ea 56 54 36 9f bf 
                    f8 6f 52 a0 5a c4 69 b9 8e 6d 4a 1c 72 fe 49 e0 
                    11 ca a1 37 06 de af da b1 36 1d e6 a9 5d 93 0b 
                    bb 4f 09 e7 fe ed 7f e0 65 68 3a 1f 8f 7a d4 b1 
                    27 34 4d 4c 49 00 30 aa 0a 01 c3 6a 82 d3 01 8b 
                    ef d5 e1 6e f1 24 42 fa 1d 48 e6 f8 e1 6a b7 46 
                    19 d0 9e 5e e4 dc af 2f f5 6e 70 1a 1b 83 7a a7 
                    83 07 6c d8 bc 79 04 e4 98 4e a4 81 1e 9d 51 a5 
                    31 9b 9c 79 b7 dd a5 bc 90 bb c8 f4 30 71 fb 29 
                    5b c7 59 f0 92 78 e2 38 fe 31 ad 47 11 ad b7 0f 
                    14 a9 3a 2a 9f 6f 2f 96 f8 22 1a e4 2a 9e a6 a2 
                    0a 28 8e d1 c7 32 0b 78 fa 7d f6 bc ac 74 59 80 
                    cd 46 18 bb 99 31 d2 03 b5 42 48 f4 65 65 7f 07 
                    e6 a1 5f 05 c7 de 32 15 84 e5 32 e1 ca b1 2a d7 
                    bb c4 75 32 82 62 f6 49 1f be 0e a7 98 3e a0 04 
                    30 54 d7 5f 94 ac df 68 b5 73 2e 9e 97 55 2e dc 
                    af 78 7e 02 a9 3c 6a e0 3c 2d 51 da 27 c5 dc 69 
                    13 1c ff 46 99 ed 44 fe d4 63 c8 d3 da fe b5 84 
                    81 76 0c a8 7e b4 3f 00 98 64 8b 0e 49 a3 ba a3 
                    a3 66 13 a5 51 62 e4 93 e6 3d 23 8a 74 ac 45 46 
                    86 87 7a 63 c2 b1 29 30 34 6a a8 8e 37 a8 74 c7 
                    70 ae 8c 95 d1 88 a3 54 15 af 97 9d 10 97 d0 0f 
                    23 c3 eb 92 dc 32 34 36 05 7f bf 80 5c 2f b0 cb 
                    d8 86 6a b5 a7 71 f9 dd 47 09 a1 3d 9b c5 16 b1 
                    8b ff 44 9e 0e cb d4 bd 8e 95 f9 80 10 0d 5f 57 
                    cf b0 ab 4a 1f 3c 1b 22 52 70 5e e0 ad 5a c5 94 
                    e2 93 fe 6f 7a 4b 18 10 6d 1e 22 f2 4b 17 cb d2 
                    1c 91 b2 6f 43 61 57 ef 72 a0 88 6b 94 1d 9c bb 
                    3a e8 33 3c a6 98 f4 f9 03 81 94 3d 20 81 6f 00 
                    37 e1 02 21 a6 f2 70 a8 53 8b a8 e4 54 68 24 32 
                    cd a5 69 6c 5d 83 ba b6 03 34 18 7a 1b cf 65 2b 
                    3a 7d 42 7d 87 22 a4 95 51 43 e2 7e bf c3 2b 1a 
                    fb 71 64 57 85 97 06 f2 7d bf cb 95 6a 5f 8c a4 
                    3f 7c 12 3d 22 2d b3 b9 18 7d 0c 5c 6f 88 5b 31 
                    b3 84 0f 40 83 72 45 bf e5 ec 98 a4 2e 44 6b aa 
                    a7 db b1 3b 4e 57 52 4b 48 7b 6b e2 2d 61 73 45 
                    bc fa 29 f7 a9 d6 7b 49 b2 72 a6 ef a9 62 5b 98 
                    63 2e 95 2b 9b 40 f1 83 bf cb 82 7d 5d fa 48 2d 
                    7e 7d ee f0 77 c9 ed 0b 46 e1 81 4f aa 05 50 70 
                    31 49 12 75 f4 e8 f8 40 51 2f 8c d9 14 5d 2d e7 
                    47 4f b9 d4 b9 18 0a d2 cd f3 fd 41 7d c6 a0 3e 
                    07 3f 08 ba 05 24 5b f6 05 a9 ad 43 3d 42 a3 7c 
                    a1 07 d8 e8 34 3b c9 e8 e1 06 44 9b d4 96 cb ed 
                    cd 26 9f c2 7e 35 c8 18 7a 7a 25 1e a6 67 4a 7f 
                    a3 f6 12 ea ff 10 74 85 b1 a8 76 38 6b 46 5b 39 
                    a4 4c 01 ee 73 9c cc 82 79 99 04 a6 bf 93 01 39 
                    63 36 f9 76 7a de fc 1e ad f2 9e 5e b8 5d f1 8b 
                    b3 1a c0 6a 72 14 09 ae dd 17 9a 19 a9 e7 32 b1 
                    4d 02 4e 20 fa 1e c5 c8 97 46 b3 01 e1 df 46 eb 
                    c1 e7 22 ab 86 29 8a 29 64 0d fa 41 fd c8 59 96 
                    c8 e2 ef 51 84 f1 f6 97 53 b2 9c ee 09 eb c9 8e 
                    83 c1 64 04 85 c9 f7 23 2d ef 2d 96 8b f3 2f 32 
                    ed 2b fd cf 1c 60 32 19 45 38 0a 10 52 35 2b 08 
                    c5 73 de 2e bc 92 3d 98 03 58 5e 37 ec 31 66 9f 
                    e6 2c 8d 04 4c bb c2 4a 19 3d bf ad d3 5a 82 78 
                    c2 de 1b 1d bb 12 af 74 13 a4 19 78 b7 ed 18 9c 
                    a8 7c f9 d3 03 54 51 55 5a d2 79 f9 67 f5 1c 56 
                    68 96 00 66 0e e0 85 a4 59 55 71 d0 9b 06 b6 05 
                    91 31 f7 a7 41 f4 3e 44 c8 a0 2d 90 95 13 6f 40 
                    41 98 d7 fc c3 02 5a 94 ba db 0c 1e cb ea 7b 0d 
                    96 97 d3 71 48 dd 2f a6 60 69 e2 52 bb 7c 01 1b 
                    ab 0d d4 25 41 c9 0c bd 8d 0f 4a 2d 0e ab c4 79 
                    36 71 37 01 1d ef 7b 2b 4f 45 5d 75 f7 0b 1f 01 
                    22 6d fd e4 d9 9b e8 dc 3a 97 13 95 5f 93 90 85 
                    bd 7b 4c f6 ed 75 ec 6a 65 8b b8 8f 8c 83 84 3f 
                    39 c7 32 44 46 3f 62 ed a1 86 2a d8 63 a4 8d fb 
                    5d bf 0f ac 7c 75 3f 6c 1a c5 0c 49 ff b3 b0 31 
                    c0 1c 76 7c 84 91 e5 6e 75 d9 d7 25 d5 84 1e 93 
                    4b 19 8e 97 23 9b 89 0e 8e 2c 59 9e 7e ad 1c 66 
                    22 b4 4e bf ce 84 be 23 e6 a9 39 04 a5 35 ef 74 
                    01 99 73 5e ae af 65 a5 7b 04 2c fa fa f6 a3 0c 
                    29 41 d1 40 58 42 5c ab e9 18 ec 7e 6e 0d b3 88 
                    75 71 df b4 fe 43 e8 11 3d a2 9f 1a dc a8 03 1e 
                    e3 21 48 f8 19 f4 97 47 ab b8 fe 12 cc 44 bc aa 
                    af ec 22 dc 55 65 31 88 c6 3a 7a f7 27 39 71 2e 
                    56 ba 8a ca 0a 31 e0 08 3a 0a 5f 05 d3 c2 60 74 
                    5a 18 14 a3 14 60 bd c0 b1 46 fb 30 69 1a a5 3c 
                    93 11 c3 7a 88 35 25 ef 9d 23 50 43 68 0a bd 4d 
                    f8 96 5a 92 93 47 66 6c 28 48 c3 83 80 4c 96 e7 
                    2e 4e ad 0f 8a 27 b8 d0 0a 90 cb e6 a8 28 d3 54 
                    5b 8c d6 e0 32 d9 2c 25 7d 87 57 ff 18 9f 5e bc 
                    82 a6 fb 21 2f f5 1c 76 e1 72 67 68 11 26 a6 99 
                    87 5e 98 d6 7a 4c 12 2e d7 15 27 fb 7c 2f a3 fc 
                    bb d5 8b 43 e6 e3 5a 01 69 5c 9b e7 39 bf 2f 9b 
                    e3 7e 7d 63 c6 ac d7 05 38 44 2f 29 10 b9 6a d0 
                    75 eb e5 de 24 d9 06 03 32 e2 de dc dd 5d c7 19 
                    d5 b3 1e 8f be 83 65 7e 19 77 be 0a 17 27 2f aa 
                    60 93 36 7a 77 76 3a 07 e2 1d f1 ae 44 d4 81 bd 
                    c4 be 9a 03 e3 b4 58 8f 93 b5 b2 7d 52 de 75 80 
                    44 d2 c7 98 76 36 3f 26 31 f9 b8 56 35 a9 a6 5b 
                    66 d8 f8 15 6a 95 02 42 dc bc 0b be 9c 54 20 f6 
                    71 83 c0 db 50 32 d9 db 95 e2 00 92 35 32 2d cb 
                    2a 89 f5 03 e4 1d 54 b1 99 84 c8 02 9c 01 7a f7 
                    f5 d1 0f 33 07 d3 69 36 f4 da e0 05 e2 22 e6 e3 
                    ff 8c dd fb eb 2e ed 6f 8b 68 b1 d2 5f 90 a7 be 
                    5b a8 0c e8 71 13 2f a5 3d d5 c0 1b f2 70 31 a1 
                    d9 b8 04 5b 77 ae 2e dc 80 e3 9d 4f 8e eb 47 bf 
                    17 00 6a ce 54 b9 7a 66 2a 91 df 0f e2 04 b3 68 
                    93 4c a9 45 3e 22 2b 2c 71 17 79 07 4c 0c a9 cc 
                    1b a0 e8 d5 c5 e6 67 01 30 49 81 1a 1c e5 ce 48 
                    bd cb a2 ee 89 00 fe cb 61 39 a2 92 8f 17 a7 2a 
                    ac eb c7 c9 25 c6 d8 83 be 83 d4 13 67 7e 2c 05 
                    2a 49 00 b4 ba b4 00 c2 14 73 cc da 66 53 92 13 
                    3b 66 ae b9 af 31 ca ad ec e5 dc f7 66 90 d1 c3 
                    bc 24 60 d8 f2 af b4 34 55 c2 81 47 f5 ac ad 84 
                    07 b3 da 4d 1c 78 c2 0c fe 93 d2 30 d6 fb 22 73 
                    d3 a6 69 a2 54 0f f8 ea 66 07 6f fd 79 b7 31 23 
                    be 8c 81 f6 b8 97 45 48 de 03 33 0f b5 67 c9 6b 
                    8f bc 9f 22 35 ee 70 b4 98 17 b9 bb 1d 7d f2 6d 
                    42 bf 10 19 62 36 0b a5 35 52 70 7c 5d 30 0d cb 
                    5d 98 c5 76 a9 5c be d5 0b 65 03 7b 8f 85 9b 15 
                    bf 5b ee fc 00 a1 07 53 9a 00 36 59 84 6a 1a 9c 
                    84 ee 59 81 36 0f fd 4e 4b 51 70 9f 7d 00 89 c8 
                    72 91 d8 e9 ee 4c 1b c7 f0 e9 87 31 2c a6 15 ad 
                    f8 40 71 bd 6f 42 aa 67 f8 74 56 44 15 cc b8 a9 
                    d9 cd 31 c5 e9 64 1b 68 b3 2e ef da d2 8a 12 4b 
                    f0 a3 98 0a 17 a2 20 94 fd 64 94 be ee b0 ac 6e 
                    5d 02 cb 24 44 a0 cb 1d b5 5d 68 d9 a2 e9 44 a7 
                    01 6f 29 77 01 ab 2e 63 f7 d5 ee 2a 45 c2 52 43 
                    f6 fa 74 de bf 3a 1c 79 bd 99 b2 79 81 fb d8 fb 
                    c4 ae 9f 18 31 f7 01 f1 a5 c7 4d c0 37 96 69 4c 
                    fa ef 30 ba 9e a3 96 08 6a fe c9 1a da 44 dd 81 
                    59 71 26 82 82 1b ce 50 4e 7f d8 7e 87 ca 7a fe 
                    f3 18 29 46 69 1e eb 73 6c 36 3e fd e1 0f e7 20 
                    78 7a af c1 40 d2 c5 29 14 17 b0 1a 63 b6 b4 ec 
                    27 b3 a6 b9 d0 a8 10 4d 64 54 3d f3 47 99 9e e8 
                    12 ab a0 49 23 e1 1e ed 4b 3e 01 ff c7 fb 95 dd 
                    9f e6 51 1d 9c 7a 32 4e a2 bc fd 92 e1 d2 ad 40 
                    42 41 ef fd 50 f6 a9 b6 d5 bd 30 cb 00 85 af ed 
                    9c 9f 43 e5 40 8e c4 48 91 44 ce c5 d1 24 f8 bf 
                    ff 3d 05 f1 d9 db 78 57 7e 2a ae 2e 48 df 62 f9 
                    d0 e4 b0 0c 58 a6 8e 5e 9b ae 9c f0 57 44 44 33 
                    90 f4 15 62 da 8e c3 d4 41 09 85 1c 40 2f 80 19 
                    bf 74 98 2e 98 00 f7 6a b0 57 2a 4f 26 6f 54 33 
                    30 8a fc 16 62 f5 1e 45 a7 f3 ed d1 32 96 eb e8 
                    6f 26 be 3b d7 86 79 56 f9 b8 b1 19 f5 47 10 76 
                    a0 29 09 c8 88 d5 3e cf 76 ee f8 99 ca 19 52 17 
                    0f c7 ab 7f 89 21 a1 ab 0f 02 9c 67 f8 f9 21 f0 
                    b5 3f f4 e6 04 06 4b 04 c8 26 75 bd b5 78 45 ba 
                    d5 5c b7 24 59 a3 69 86 f6 32 c6 e3 e0 eb f6 24 
                    34 3f 29 fd 36 5d 7a fc aa 79 cc 70 47 dd dc 3e 
                    b8 fc 1f 75 ba 76 52 aa 50 fd fe 2a 0f d7 fd 07 
                    46 fc 85 cb f8 08 8c 00 50 fb 8b 31 b8 a8 93 5c 
                    79 2d f8 e6 91 d1 bc 67 a0 87 b2 7c 85 19 24 d0 
                    9e 2a 93 7e 4b 72 cb 67 65 2b 5a 29 92 af 37 c0 
                    dc 63 59 5f ed 38 13 a9 32 30 55 2f f6 3d 83 4c 
                    8a 5c f6 ff 19 cf a9 06 a2 d2 af 85 4c ce 00 ef 
                    36 66 13 f7 2e f9 06 c4 33 0a 4e b7 45 24 c3 26 
                    e0 a4 45 83 b2 42 4f a0 b1 60 4d 9a 4b 32 a5 e4 
                    31 d2 e1 6b 9a 30 f7 4a 27 de 54 44 6b d4 ce 0a 
                    e6 78 7d 3d 25 aa 2f 62 0d 7c 03 6e 11 9b dc 7e 
                    db 2f 9a fb 51 5f 91 29 be cf ee bd 34 74 46 8d 
                    f1 63 22 9c 17 65 dc 63 78 2f fd c9 9e e8 32 f9 
                    27 39 24 a8 a4 2b c5 87 fd 3a 55 80 18 37 fe dc 
                    db 70 1e 32 87 79 5e ad 3c 0b 75 4b 42 39 6c 72 
                    20 2f 98 e7 3f e8 4d 25 1c c9 40 f1 9a 40 f7 55 
                    8f f4 e2 20 1b 91 6f 5e 6a a7 a7 81 73 ae e5 b1 
                    2a fb 26 14 43 c4 11 77 ae 79 cc bb 1d 6f 6a d9 
                    d0 26 86 2e 3b 32 ef 8b 2d 3b fe 9b 26 90 2d bc 
                    d6 7c ac df f5 da bd b1 30 c0 a2 7c df a8 c8 b7 
                    33 49 05 c8 b8 a5 b7 1d 3d 31 cc ea d2 2f 9d c5 
                    d7 30 81 cc f3 2e 72 81 6e f6 32 3d 01 29 66 03 
                    b1 5b df 33 5a ba 65 cd b0 96 aa a8 95 12 6f 98 
                    6e dc 3a 3d 17 ca 48 21 13 56 fb d7 e4 84 30 9b 
                    d7 81 49 fb bc 05 db 6d 7d 3f de 38 f9 ae 44 61 
                    4e 0e f1 05 bd 40 92 37 6d ae 95 51 d9 6f 3d 0c 
                    9d a3 e9 c3 b7 0a ac f1 5d 7d 18 42 4a 77 84 af 
                    6f ae 1f 71 91 c3 40 14 e2 42 89 12 02 d4 7e 84 
                    ec 74 9c a2 19 88 00 e7 c9 8f b2 08 36 5b 9f 6e 
                    a0 3a 83 b2 eb 5d 19 ef 0a 43 b9 f0 52 55 f3 ac 
                    b3 28 65 c1 91 a3 9b ef b9 8e 86 1f f2 a8 bf 80 
                    61 5c c5 0c 9d 2a a3 c4 52 61 d7 95 89 95 12 43 
                    5f d3 b2 f9 fa 48 1b 27 3c 37 52 62 d4 ff c9 43 
                    f1 cc f1 96 ac 7a c4 7d 28 22 3b 03 c4 ae ab cc 
                    2a 62 f3 45 1c 5c 8e c2 36 5a 35 cc a5 b4 18 0a 
                    d6 bb 4c bd 60 bb bb a6 86 4b 80 7f 39 f1 99 73 
                    df 6f 48 8a 21 18 ad 70 2d 4d 54 f0 3c 90 b4 55 
                    ab 8d ee 53 9e af 4d 5f ea 02 60 a6 d5 92 56 8d 
                    10 9e 58 f7 27 03 c5 b4 7c a6 63 92 b6 c0 35 93 
                    0f 63 c5 de c2 f5 4d 99 33 f5 29 eb 35 6e b2 ac 
                    6e 78 26 3b 48 41 f1 93 93 73 91 b3 05 c4 b6 08 
                    7f 37 f6 08 a8 94 31 ce 9c ec c6 a9 d4 e1 6c bc 
                    58 eb 1e 4e 06 76 8f a7 9c 66 8e 2f 0f 16 d5 e4 
                    42 76 19 46 15 dd 97 b8 a0 ad da 51 3a ca bd 52 
                    d0 6a 6b 25 69 8a 36 05 37 26 cc 51 57 bd 45 a9 
                    a2 94 de e9 5f b9 24 8d c7 63 bd 7d 6b dc bf c9 
                    6d 90 49 45 89 58 60 ca 2e 95 61 b0 b8 ee 3f 7d 
                    40 34 b5 04 5c 30 52 4f 06 7e 6f 10 1e e0 0f 40 
                    dc bd 22 a2 0a 18 56 32 a2 aa 45 89 44 5b 43 91 
                    2a 7c 77 0f 47 8a aa 4f 81 69 71 85 b0 dc 01 03 
                    3f 88 9c 3a 64 96 27 0e 7c a2 92 5f be b7 79 b2 
                    0d 66 dd 83 20 9f 93 de 75 1f 04 cc c1 62 78 85 
                    48 c4 3e f7 64 04 45 61 c3 3f ea 53 f1 0c 7d 13 
                    42 e1 bf 9c b2 1f e3 15 3e ec 40 a8 0c 54 78 ec 
                    6e 88 a1 b2 4e 9d 33 d6 a4 54 27 15 89 4d a8 cc 
                    da ee 13 58 6b da 62 05 13 72 f3 2e 21 25 f1 84 
                    1a 8d 8c 04 93 d9 9b 1d 98 1d 09 47 c9 93 55 88 
                    da 5d 63 9e cf a5 ef 38 8d 77 db 82 01 96 44 33 
                    db 00 21 cd 94 e5 0f 5e a1 6a 54 2b 56 8a d3 5f 
                    1f 9a a5 0c 98 f8 f3 c2 d8 36 63 33 84 e8 ac 4a 
                    7b 2a 33 a6 db f9 35 35 b3 e0 36 77 29 0c ef 0f 
                    b4 73 17 7c 50 10 75 7a ca bd b2 b8 5e 53 fa 07 
                    e4 18 92 6f fe 2d 3c fd ab f7 49 22 f8 92 fb 22 
                    05 86 6d d6 5f 0b 26 65 c8 f8 4c e7 26 c3 bf 15 
                    79 94 29 1d 3d 02 44 88 a5 0e 15 93 c2 ff e7 55 
                    fd fc b2 6e 16 cc 19 72 6d c3 0d 38 9a ef 53 36 
                    a2 1e 91 59 f6 d4 f8 c8 6c 9b a4 5e 1c 22 d6 b7 
                    76 11 c9 d1 33 6e 02 94 39 c2 de e7 18 2f 67 fb 
                    a4 78 f7 e1 92 66 6f 2e b8 9c be 16 ac 78 78 64 
                    64 2b 24 3a b5 7d 33 15 c4 70 61 09 df 2f ee 55 
                    ee 4f 4c 6c b3 5e 1e 74 43 42 34 5e 56 d5 02 5e 
                    09 de 2f 8d cf 07 a0 22 69 9e 35 18 db 4c 20 c4 
                    73 72 93 e6 3f f6 4f b9 54 36 0d 55 a1 8d 66 8d 
                    4b fe 52 4f 57 5f 44 20 df 5d 18 81 85 14 3c bd 
                    4c 25 25 de 62 25 2a 96 a8 a8 06 f7 54 d6 c4 26 
                    9c a5 5b 93 79 7f 62 22 ca ef c8 5d c7 0f 00 78 
                    52 9d a3 8e 13 d4 3c 55 9c cc 4d 67 10 32 d0 2d 
                    1e c3 1e c0 b1 0d 12 08 2b d9 b1 8f ee a1 02 da 
                    89 74 f8 fb 96 59 88 59 63 8f 16 6e af fa d1 b4 
                    7e f7 69 24 fd 79 76 62 1d a2 5c 52 0d 02 ca a8 
                    fa ad 84 57 81 31 b8 ca f8 a9 65 c9 6f 41 2d 2c 
                    95 e3 0c 1a 78 55 49 a6 42 75 0b 8a e2 c2 1d 1c 
                    2f cc 25 ae ce 06 cb c3 c5 3a 7a 84 99 b0 0c 73 
                    5c 03 44 cf 5e 9e 29 cb 9a 35 ec 57 60 20 02 e6 
                    9e 0f 66 70 07 64 e2 43 ba f5 75 ec 44 e5 ec b1 
                    20 c5 82 de 69 44 2b 97 13 31 94 52 b3 47 8e ee 
                    b5 a3 1f 6b 70 9b 6b 16 9b 73 b7 01 6c 63 ec 12 
                    cc 9f 98 17 ca c3 97 50 9f 45 66 4c 8f 4b 45 7b 
                    aa e3 87 2f 04 00 be 69 24 ce 8e a9 b0 bd 41 20 
                    3a 1b e7 9b 24 80 b5 d5 ce 74 75 fd 2e ef e9 0e 
                    26 40 40 11 d2 b1 21 bc ab 26 8f c7 81 f6 55 fc 
                    cb 12 45 2c 0d 6f 0e 2d af e7 01 de ca 17 4d 76 
                    53 21 18 56 0d 41 ef 77 87 66 4f f1 b9 ae d8 2f 
                    72 85 0f 6b 1e a1 4e d9 49 bb a3 05 b3 a0 83 91 
                    d3 00 a6 07 f0 d5 9b 21 83 cd d9 4c 79 4e 54 2c 
                    62 73 bf 1c 81 d5 39 0a 8e 0e 2e 0d 48 7c 57 60 
                    46 30 4b ec 3b 5f dc 84 9f fe 88 72 42 20 57 c9 
                    f5 fa b6 c8 b5 d7 6b 5f 5c 8e 1e a1 fa de ce ea 
                    53 5e b3 22 c5 9e 02 02 ae 64 e8 6c 98 18 bd 20 
                    a5 6b 03 a2 6f 8b 73 35 0a 2d d5 8c 48 07 c1 ef 
                    40 11 8e b3 c5 bb 2e 50 28 a6 6d ae 9e 89 ae a2 
                    97 bc d3 c3 9e 62 71 11 8e df 50 5b fc d9 53 29 
                    48 1f cb 50 75 e8 2d d7 7f c9 d2 c6 fc f8 c6 c9 
                    b6 51 bc 4b 58 03 49 c8 ec c6 a4 43 dc 63 35 eb 
                    24 bc 8f ab be 81 3b 8d 45 df d6 f9 aa 16 c4 c2 
                    97 3f d5 df 6c 87 70 76 53 96 8b a0 dc 76 df 74 
                    0b 0f 95 c7 0f 1b ad 67 b7 57 8c da 53 94 69 09 
                    f2 35 5d a5 b7 b9 a5 c2 fa 90 24 79 56 1c 0b f1 
                    55 65 d0 fc 0b c6 9b e6 e9 89 4f ac f3 4c 19 de 
                    f2 47 31 4d f0 0d 93 77 e5 3a c8 7c 09 b0 6a ff 
                    86 bf 1e b8 cd 1e 83 f8 9b b4 1f 4e 35 ea b7 a5 
                    ff 73 79 46 e2 89 ba f1 e3 23 5d 92 53 8d cb 85 
                    c8 71 61 82 3f 8e 12 0d d7 e8 01 48 92 95 a4 d5 
                    08 03 b6 b2 da 42 f8 26 e3 f9 7f f0 f3 4f d0 ca 
                    7c 22 e3 28 ed 44 62 e9 78 76 ae b6 80 7a aa 05 
                    09 2e 3c 5d a9 8c c7 a4 8b 4c 84 10 7b 13 8d 07 
                    9d e2 c1 95 7f ec 00 40 66 a9 01 00 45 bb ec 71 
                    ed 4e c4 fa 2b 58 b5 19 a7 43 5b 4f 8d d0 b7 aa 
                    b8 38 a5 8c 01 58 6b 23 fd 16 b4 b3 d2 fc 4e 13 
                    c6 4c 39 b8 b6 64 b3 14 55 f7 89 32 b1 43 1b 3e 
                    fe 76 2e 98 2e 2b 31 e3 98 12 6c 76 4a 85 19 3e 
                    bd 2e 6d 47 ab 8f b1 85 1a e4 02 03 f0 10 42 13 
                    f5 1c 2c b2 ec 05 80 02 95 fd 55 fa 65 c3 96 0d 
                    12 13 0a 5b de c3 94 0c df 50 b1 2d 17 a5 b4 d0 
                    20 2e 42 75 6b 4f dd 28 51 9b d6 cd a1 8d d2 72 
                    bd 4a f9 aa 40 00 94 92 60 c8 d3 cd 76 75 0d ae 
                    90 4f 89 66 ac bf a3 f5 65 d0 d8 a9 53 4a f3 d2 
                    b0 a8 62 b5 b5 dc 48 1a 6e e3 b7 95 b8 ad 2f 75 
                    5b 1c ab 2c ae b9 2f be f9 40 0c 95 65 e5 4e b2 
                    c5 8d e6 d3 e6 55 3f d1 24 82 11 ca 45 9d cc 9f 
                    f8 05 61 08 e1 17 97 46 31 04 d1 a0 b3 27 04 0b 
                    94 d1 ac bc de b4 4d 6b 7b 17 5d 00 4d 78 c0 a7 
                    a8 e6 8f 37 67 aa e8 7a 46 96 84 04 c1 fc 56 8e 
                    fc 27 8c 67 07 ea 77 0b 15 61 65 8f 27 44 d3 42 
                    7f 1d 49 84 40 db f7 56 a3 25 80 66 0e 31 5b 80 
                    35 e4 85 3a 1e a6 2c 32 71 fd c6 db b4 bd 02 c5 
                    20 63 b1 13 9e 0e af 98 03 53 5c e1 98 f5 60 92 
                    14 d8 7e ab 03 d8 1e 27 d1 cd 17 f3 6f 98 7d e8 
                    a4 8a cf 86 01 65 a6 8f 76 4b bd d2 bd 5d 80 78 
                    9c d4 16 73 c5 3d a5 fd ca 8a d3 41 39 24 7b 9c 
                    d2 68 39 90 18 70 0e 21 af a9 e1 42 57 a2 a9 7b 
                    3c 74 2b ac 23 1b 33 91 5e 20 4a ce ea d4 ed 49 
                    65 b4 c0 13 9a b4 b2 8a 1f 84 d4 df 4b a7 e8 9f 
                    48 96 09 61 52 58 7e 78 0a 90 78 16 c2 31 fc d9 
                    5c 4e 7b 7b c7 f4 6f 85 1c 11 93 09 c5 d8 07 11 
                    e5 23 09 48 6e f3 d0 f4 9c 02 e7 66 89 fd 9d 76 
                    ab 86 ad 00 04 fd ec 9c 2c 70 e7 92 78 4b e9 3b 
                    e2 1b f5 ca 17 05 66 7a 38 04 a9 fd 94 ed 63 00 
                    0e 9b ef 2f 24 56 aa fb 1d 68 20 c0 4f 8a 6f f8 
                    cf d0 70 71 7d c1 2c 52 9d 73 1d 5f a4 f9 b0 80 
                    ec 87 1d 36 05 f0 18 36 69 08 1b 8c 1b 89 f9 de 
                    5b 0a 21 e6 4d 3f 06 1d f9 e0 70 2a 25 6d a4 55 
                    16 b8 69 c5 ba c5 ff 2f b4 f3 fd 2b f6 df db cb 
                    ca f3 86 80 df 79 17 a6 f5 ae a0 ab 60 f4 11 ca 
                    b9 71 fb 84 0e a1 5d 93 30 19 13 9a 07 6c f9 83 
                    c0 74 85 f9 0b 04 56 49 d3 d8 ca 95 98 c7 c1 e7 
                    71 f5 ab a6 5a ec 9d 59 40 0e e3 b8 e1 ac b9 73 
                    e9 36 a0 1d 96 ac 8f 3e bb 30 55 b8 e0 41 24 03 
                    c5 ab 29 a3 3f a3 21 6d 01 f3 9d 4c a2 1a 1d cd 
                    c3 d1 99 4d 1f 0e e5 e4 08 f5 a9 76 fb 63 3c e5 
                    ac 94 3d bc 00 c6 c5 f4 5d 86 7b e7 53 c7 77 ef 
                    1e cd 7e d5 a7 23 ab fd e4 ba f9 3c a0 5b 65 59 
                    29 70 75 a3 c6 6b 7b ea 8b 74 62 93 bf af cb a3 
                    77 b2 69 a8 98 17 36 63 1a c6 68 9e 59 47 eb c5 
                    03 f2 ee 63 19 70 9b 68 db 33 e4 3d 80 76 ba ce 
                    7d 97 a6 c8 bb 22 e1 c4 e3 8b b8 6e 36 59 f5 ef 
                    da 4e de 39 b5 a1 86 46 9e 00 9d 60 b7 c4 ca db 
                    19 a5 71 e1 94 96 d8 1f de 18 ec 01 e4 01 3a bb 
                    11 91 8f 25 0f 93 7c c5 86 1a a2 bf ab 4b e1 c2 
                    f2 80 d8 d9 f1 d3 b7 55 c0 45 41 ea 5b f6 30 41 
                    26 b0 b8 a9 cb 82 2b c5 de d3 6b f4 fc 36 c5 08 
                    64 25 87 be 16 92 ef f5 15 51 ee bd cd 9c 6b 8e 
                    03 58 ef 47 d5 e1 7b 85 49 d5 12 fb b9 2e 48 41 
                    03 38 d5 79 ac 72 00 20 f2 d4 71 31 0a c6 1c 59 
                    74 89 d9 ea c0 75 77 7e d1 a3 1a 5e f0 5a e3 34 
                    70 5d 02 89 de b6 4f 78 89 44 cc ca 2e 98 a5 6a 
                    c0 56 a2 7f 68 55 d3 bd 52 7d f9 12 d9 1c f1 27 
                    50 db 14 55 2c 05 ce 7c a8 1f 66 0a e4 99 70 ad 
                    17 2c 6e bb 8a 2f a9 c2 84 d3 95 dd 09 7e cc ca 
                    56 ae 65 14 d7 9d 4b 0a 5f 05 a6 75 41 3b 97 2f 
                    93 4c ce 7a 44 f4 80 f2 9b d8 42 7f 6d 36 6f 24 
                    8c 44 0f fa d0 4d 33 66 8f 32 b8 42 73 37 7a 42 
                    3d 63 7f b5 de 8e 4b f6 a8 21 05 63 57 a5 f3 2e 
                    9b bf 31 e7 0d dd 14 cd fa 97 2c 48 bb 14 cb 37 
                    d9 bf 81 e5 ad a8 08 45 49 ac 1e c2 f1 ec b4 e9 
                    e1 27 09 a7 75 24 24 04 75 d8 1b 39 a6 66 1d ad 
                    80 28 21 82 87 e1 dd 3a af be fb 77 a0 35 9d 32 
                    98 fe 0f f9 5e ac f4 c8 da ef fa e7 47 25 7a f2 
                    18 d3 34 83 25 a2 80 4b d6 11 a3 71 d1 0a f3 79 
                    aa 0d 39 f6 26 cc 91 ea b2 82 01 46 62 dc 6a 1d 
                    b3 a3 9b af 10 5b fd 45 29 fa 4e 92 ec 3c f8 6a 
                    c4 3f d2 82 38 ce c8 4c ef e9 b0 29 55 f5 45 f4 
                    57 73 1b cc dd 6a af 8d 01 d0 c9 b8 d4 0f 53 77 
                    e4 ba 34 53 72 85 23 73 af 2e d0 07 f9 2f 33 64 
                    33 2e bb 91 d8 ae 64 8e c2 a9 5c 0e fa b2 b4 6b 
                    16 0d 0b 3e 0a 78 bf 1e af 64 c9 51 6a 6a ac ad 
                    a4 2a 53 c0 f4 27 13 10 41 e2 36 ed 06 15 26 4f 
                    4c 50 76 37 9c 07 0e 95 e4 8c 2a e2 6c ec 45 52 
                    84 2a e7 49 ed a5 d9 ad cf 98 8a 9b 7f fa 43 5f 
                    60 80 64 00 8f 0f c9 39 29 82 30 eb 1b 37 a2 a2 
                    11 df 90 75 05 48 85 d6 67 b8 e5 45 61 86 30 d0 
                    46 5f a0 66 47 50 76 79 cb 6a 64 a9 50 49 d3 96 
                    09 90 31 a9 00 30 f7 8c 80 d5 6d f2 2a cf 3e 86 
                    24 8f dc bc be 88 d6 1f 21 a0 ca 14 cf 8e 7a 98 
                    37 d6 aa f7 07 50 b7 fe ab 0e 14 7a e0 9e e7 06 
                    9f bf 8c fd 35 40 94 d4 7b c8 65 16 38 31 c0 b5 
                    a9 0c 9a 5e fd a4 7a ea a5 ff 6a 53 55 e5 04 79 
                    c0 ea a9 5e 2b 5c 94 99 fc 68 07 80 e9 80 44 3f 
                    14 e4 c7 a0 0f 8d 8e 52 6f d3 bb a9 cd cd 93 82 
                    4a 9b 95 bc 98 aa 8f 9f 78 25 de 82 b3 41 0c 0a 
                    2a 3c bd b4 d8 97 e9 a7 eb 51 48 8e 39 5b e1 3a 
                    eb 1b 16 2d c3 99 c2 cc 0a e3 8b 9c 3a 9e fe 73 
                    f6 17 d6 b7 61 76 3e 7c 47 5d 09 fb b3 4e a7 ce 
                    b5 ad 32 96 89 b6 8d d0 a1 5a f2 ad a7 56 55 60 
                    18 6b 44 12 73 3d b4 b0 db f2 e0 9e da e2 69 5f 
                    89 49 55 b6 a8 04 bb 06 e8 50 fb a5 7b 5d 1e 80 
                    9a 51 ae b8 a0 82 ad 9d 42 d7 10 68 19 f3 17 c0 
                    34 24 fa 0c bc 12 0d 6e b1 f4 96 f5 c5 a8 f3 a9 
                    53 cc 7d 08 a0 72 1d 93 8e ab 5c 03 bb 2b fb a5 
                    36 34 9e 18 56 90 54 1d 0c 8e e4 49 a4 5a 90 a7 
                    3b 7d 51 94 5e 9c df 97 cf ce a3 e4 b1 46 c5 9b 
                    4e 4f 45 17 aa 64 c3 a0 83 df 97 c6 ea a9 9d a3 
                    60 3d 52 fd db 1f d1 9f 53 62 7c 24 01 bf fa 93 
                    77 3a 83 ec b8 5b 5b 5b 53 5c 91 b4 64 29 8d 45 
                    b1 e4 50 5c 05 05 bc d6 36 d4 e2 80 6a 90 dc c1 
                    5f 8b 6e ef d2 c9 67 2c 3d 60 80 d8 58 3f 4c 97 
                    50 66 5f 06 fe 86 34 f7 b6 2d e2 15 d6 e7 27 39 
                    e1 4b 54 69 d8 57 5c dc e8 57 c1 ba 0e 0c 21 0f 
                    49 51 f1 7c 8d 57 7d b4 2a 0c 07 98 30 24 c6 36 
                    97 36 2d 43 da e0 d4 5e 06 77 b9 1d 8c d5 51 f9 
                    e9 47 14 34 6d 22 66 49 5d 16 79 08 0f c6 bd a2 
                    9b 64 58 64 3e d1 0c 91 24 e9 77 f1 82 4f 06 34 
                    06 7a 5a c4 96 3a 9e e7 08 27 d1 72 0b a1 06 7b 
                    29 f5 b2 49 ca 27 fb 9a 7c 79 60 55 33 82 8a d0 
                    ad 69 c1 05 29 54 84 d5 c6 9d 57 75 2a a5 f4 aa 
                    49 9a 58 f8 36 d9 b9 6a 12 2c 37 0c dd e8 5c 9c 
                    7f 8d 9a 91 af d3 fb 2f c3 6c 93 a6 8b c4 0e 83 
                    34 93 95 e4 51 48 de a9 b4 18 5f 4a 81 29 b9 63 
                    ac b8 44 ef fd 85 e0 12 9d 87 44 b6 f3 a6 45 38 
                    60 32 21 70 dc e3 af 23 74 b5 ed 97 cc 55 1b 13 
                    8a e5 a0 51 6d 35 61 c2 7e 85 72 be 45 76 43 6b 
                    14 24 bd 86 2e 06 bb 2d 5c e1 9c b6 df 48 45 bf 
                    8b 46 6d b9 72 4f 22 61 2f b5 ea 3e a9 08 92 74 
                    c0 35 3e 3a 89 c5 ec f7 f2 9d e1 84 40 bd 66 1c 
                    97 48 73 bf 7b ee e8 4a d5 6b b2 17 fa e9 9d 3a 
                    c7 b5 a5 e9 21 47 a8 c2 ae aa e8 63 7b 90 6e 8f 
                    32 c0 49 5f 2b d5 e8 1a de fc 4b bb 6a 28 71 16 
                    54 8e d0 67 64 26 8f 4f c2 83 51 d1 e1 55 34 55 
                    0f ba f0 01 0d d1 0e b2 ec 9f b3 74 c7 b2 05 86 
                    df 77 96 e1 86 07 bb aa 84 c1 27 64 03 cc 72 40 
                    eb b9 c7 3e 8e 08 4c ef de c7 fb ad 24 d5 8d 03 
                    20 7d db 7f dd c1 66 51 66 55 69 7d 4d fe ce 27 
                    3b 13 3a 76 0b dd dd 14 a2 cb 5e ae 50 de da 7f 
                    51 e9 b7 41 4c 95 93 47 eb 6f 9d ac 01 da 57 c2 
                    29 e1 30 7d c3 2c 93 73 57 5b a4 37 ac 55 d0 27 
                    01 60 ed a2 4b e6 88 cb ae 66 ec f7 14 e5 3a 97 
                    36 b9 47 68 e4 8c cc d4 40 34 ea 9d 7c 74 bd 81 
                    24 7c 12 fa 5f eb fe 0e f2 01 04 85 21 eb 23 d1 
                    34 71 7c 50 08 59 52 12 26 b6 7f f2 c6 2a b2 a4 
                    c0 ba e7 c9 51 e6 ad f7 08 a3 49 01 8e 9c fe a4 
                    97 29 a1 79 ea ac 45 43 c0 0c 58 46 f1 cd 94 a3 
                    11 3f 98 dd 8f ef 08 64 8a a9 27 96 d0 35 c5 f4 
                    47 c4 52 8c 9c 0b 3a 50 62 eb b0 c3 f9 f1 c8 66 
                    46 58 26 02 5a 10 1d 0c 96 53 ab 3a 6e 51 b4 ff 
                    a4 db 6b f4 9b 0e 55 f1 67 9b 54 dc 7f 04 62 85 
                    e9 06 2c 58 62 c2 16 c1 b8 b3 89 ff 36 83 c4 32 
                    22 ec 8d 88 c7 3a 45 2b 6c 80 c1 5b 1e f2 9a cc 
                    51 de 8d 05 08 f1 9e ab e7 eb 43 68 b4 a8 11 e1 
                    b9 58 37 f1 0d e1 04 85 65 5f 5d 7d 13 ae 2d fe 
                    7a ce 81 e5 77 5c 79 f6 6a e3 d5 04 27 3e c7 25 
                    8f 74 51 b2 41 86 59 93 e5 e7 74 e7 2a 00 2c 45 
                    0b 74 c3 4a 9b 7a a8 7e c4 31 36 69 24 34 d1 fd 
                    c0 0b 1c e0 00 60 83 c3 5a 8b 96 62 3b e0 2b 8f 
                    11 f9 19 30 6d dd 9f 46 8a f2 af ba 23 48 74",
                    "data" => "456bd4aac8895d436043a932704794d8ffdbb2425efd195889f06a6df35fe18b7a044949c0a1e92cd56b7eb8b189980354ae295bb34c80a6463fdb04d3982494c56c37d65caee09321aca977559340672fd46024e9c776ab482805d61a7433bae2cf817623c4865fdb3d0e41ea6b319af5543a6baa34d6ca3201e88f65139f96e261dee5b43a66c052e0624b87fa4ddc8e2f3427f1ba9f7f35209fc7a0407a245a9657fca3c6c5fa433eaa5840289e39bf0737a7262a5a59630fb78dd8965ecb6229bd6e8683dd1f572b6dcb68c74d75e6d0b68733955a13afacd84a7fbdd63a3b75b8011b6af6452904cef8cdd4d6f75f0fbfd5b7f7d64bcc99a6758fe40954498ca6e7fe709ce1f614e9110c5d34246879112dd6076150b5a45bf699197c354229411206c58e20d6f3a07fb452fe3ea4e18555844170a0a5d54adda3128ac4644401dd79f78f110f49e1249ce18e24dc8b23fd2704d0ddacac7262b8857ce6af36a3737e6ce665f21662abd15eacadeb52223e8a12c74d7cdd1d800b4fe10597005d1c361363bd7976bd4eaa0f8b06f53dc756ec3f27d1e561dfb0e3e88125824cfea0369e2b0c12d909c4c4dea9f4a9af302b62d18c7833ab3f2dd3d00edfe552ba5a4a241b59fae81693a0255338acab8ee0a4470536dc5723e133a5a33cd49142a774a0249f336b8443c00ca001814a7edc70af3a32fb95e22dc65e2e024a395fbd8f54d6e74f4a3fec102ac44a7d3d566a39d1d78a6fd2c33f043493bf31a7369bfd03bd0ff1618c21c85d32a40ea996a9275599a7a0d8ba0f52c6fe336d418270ee88e280b73841f0800f8ed5c06d771640db0caac89746c50c8fbce2f21edae2c9b3514aa47864f7ce42a362864401f3f7bdabbccb804c883fdd34426a90aaabb1defb37f74e8b5bfed205898738f1f4d1b7cf8ec55bbee8fe41e96ecc42a3bd4cb68c34168f50a8209cecba3fa8d1da100afd60e7e1b0117cf67fd35fdeda55557c3294980cda88fb2b371fc9841bbbe27588303d5af6dbec00f5fb2f090c17d62bcef44ede5a96505a2d7363073b78b5ed954b45e63edbd827c13b9cf1c7a5eb153619d3c41b16b57aa2a1dd1593eb003c030d512e2b7a8bc7dfeecb3d1e6e05cfa0d0425b5c86745b691709d14878988e52c2a3044ea1730e0463024a8fd18c044f578d1a076e7ccc84de39d4cf6044004d3763577b57998e7da2c9769306a0579a585aad47eda5a189d6a59081ce15ab04764e285433bd5afbbdf06a34adff511ce5fe17574ae64ad399af8070eb9ff3cdc3843d7c089e5be7b493b600b5cf26aa425102c2f9141fee8c0bc31733806d937cead609fe818c8fca1f1cec55a1a377260dbe2e5ecba0104e3304d6d4f195ad36ca07c418433be95d7ab3292449b5c3a3e52d1e09d4c45f3f6d41623aed6cb952937fca9cd6a3a2c3c858ae8c2ec0d4203b5e8d3d2b1d51b7d6159ab585ca9acc4ac3d853e20eca486da5ade15f28c2166a3afa73bb1bdd8d41921e71b3e40899506059578ff45370c98b00960c0de8c56ef346e960b8f3ef2d2b0559b49efa2134eb589028b2e51c4c29981385f343aaab4290b945ecaa91c629ce5c56118212ef6e0c36f511154fa8cb0590c0603a4c8816fa01a7d887d39ad99c95a948c46a728c5a6f606c9b0eb72c1af3db032ae580fd404c934f986aab44dfcddc254f5c583983b43b006e8b920334633ab9076236873f2d419af6d7a2c1de99a601e77c6c96661f115b2004ee7988a131a62992ab809e1ef167074d5b5a4b8a5d1d40133a98cdb199713714cfcf092c0edca69d91fda06c212165b05d68e9911f8cd0291314d317da372a362d289288159cdf0effd0c31434d029b0f7b1c4e863fb32de055c41a6f19af5af02d519de3936cad22d5dc8e1f467c692b884628a2c696db911d3404514808f1380a37f858c7bb2ba3f63c5fa591bda4fc6e8f2329dd6261573b27f61d5de2d9e4fa7e8268aa377926173c8dec7188737b1dfc6822df20e486a19d159f989a1fcb97b97c95edb141556278c68f71af7d084391340fc75111d19aba8b5ce7712b02bfcdccd971965a0e7a07defdd3d179fe5eed116d0a9379313a85438e35f799c00b614d8b27d567e9d9d1f111d9e9d2a72a81a3f6529a61245aa6b4aad84fbcd42428ff4c1d9bd6012bc0090a96da01fa534727cc3687497c4d6d7983ffe4e3ca40fd4f694859ceaf65918377d673cc7621c4e7b90019114a90637778dfa1a07
                    80566f31a1763f38a73edb122582331e5993237fbc667357139dbd8224770dd3bc339509967e54b8c96302831913b7ef61df21ea7f2e6caa6a7ddf04d12eecbf1488f7b371181020c12b137e4e2fe974f885311f2fb3bb026cd62750e6e05747c1d3f3fe39e04e73f80fc71c319ab1351fad70bff913393c35dbbcc8152af4c4997c066d28f04461ad21b65e6ee9b233e4d20be58648357f6d1b3597f1a19f050447eb1e4f2416306232cd887d0f6bf8b5e40b4c4d0f7323301de90bfeb29fa4eec24a2b27febc2e08c2560ff4e7a610df0ab3737cf3d2fcbe67ce5732f69a962370339fb17176d1d5fb72d69a4727eec1b0916c438cad20724341a3cfb4e4108386ca4a02c511058c3c2906f4d02dcfdc9342a791dc0d3adde769123345b5c8420b4a124317e99fb32681039b62e5e0a62ff3b898c41e292d481bd19fbd4ba9d67c07b7a4fad37a558036b9d91f45e53b45e2c22b49ad94482646f884ca67c3caff4c905071fb53bbdd08277247dc9185554dae64832bb8805a6cc2f4c8c178c5df5f6aa53a3adecb2ae03d262267f8a789c30a9631fa92b63a12175887a0c35517b5445742222a0c679b53dcd24e64d1b0750ba7fa3969da713a0762e6bdf2ac89d2651ea6a3cf43c3d8edfb1405ed3c7df513ea2c3626e3c4afb427d6bad280dcf65119fc181bc2fa5062676a83fac9a976f0b05195ed4f3749fe1ecc77e2dc4800f8f39ca754ab038ed962f2d748950027bdd60e07db8fa2ed20386809844efccb062d967a519c182c7be1a08d23330f64e26214908fee138e899039136507113a79ad2ab283af9224402ec4658e9dc49ac6696cad97d98b191d77563fd588df3e209a3b655c3161b21a052d705fad8b3f548bff2b8a8f8bc6f0033c46eaecd34b7f220843dd4c8f6c61122c5b74fdb6def23cb78213ae50d2bdd3f6be35ade6f42774e7b80b8ca883ede57ff9824eece1d821d11412b01c10cab19d0c9dcdb94be565409f04fd2597750a4a4c899376a772c9ae2efc83d8bdcce67cbc6bdc7a85055b1c6e08c58d171d23a9447b6b7bf2cdbcbaa1ee5dc9d10c5cd63f8653dbd4521eeede87962f689a2e4cbc5b882cb28a8aed822b25d0adb38f058cc57e88f70cf700eea289ca8696a9c4d1c051709f3d197115a81d2699996edcc9add9fb3ec7b5a06fd05ffc12f41cbb67c0ebabe3a7e024b51f1448b3292446b54146107c8ccfa904b11d7c181e8b0a2d1231da43a0673e8da5c533fc9e329da25d3bdbbdcc0222a29a8f80704feb422b2202ff89d52cfb555f969abbe1d4c6ac5c2202b36d107aa153a301e59fa4794d0bddf7d8e50d47924d725754ff5435b36c2554cd8116838e9f5a3f2dfdb2ac11f5e55c608ce39fa6a38387f0e21c6b5f289842e2814522386fea35cc0591b428483fc4def4e1cb2ede84c9fdaebfa1b4d6915cea3da4a6a919de83c0d0f703751e8464099db21ae8080d6db7ff40df059cc574f335ce3311e373b47c96bdb93f4fa2d857cc07134b2ec250f9b7760b1fb23608aa99632502f166d773d5cce59639e95b25517d71175c561ee8cfd67d9cd4a993c36c0410bd0a47b4319e4c5d360b030ce146fda45d31503f1c37978f8357d117054d579ad8e26eb84a5960b2f64ef4461bc01c1f82323e84e5d285af42ad44592da3fe0762a3b67d3ec0d6c11513fdcc0c41e0e5ccef9e3e18d16466ac794086b008a41261e6689f565eb1a898d6557591410c98a85448e5d65cbced14996f04f41436ead7e56bfb55f95a29f69acf63514f6a0d394ef83b903c382f4f528e79e91edcf31d04c057410e35308c887d8ebb30225d5fb1446f114fb98481e5b44a1e65f82a560a3c6f656cd23258e5d89a43962d9279ec8024ed63f7554f7a6b88f5da91d933ac227d6dafc07350ac22ebaf77ff25b2e782e23527e44926d734d561ff2dead9ec8b7ee7ec1a1f6a5d32cf502c07975e1fc7fb58cb473addf9a1697a38ffbdba4ec2b9c2fcaafb1c56471a49e7f8bfe437db27f3b3257981897f813924f75112862fbc73749b8d387cf308d32efb4560ffee5657a6cbb31cc816b54861814598f0248ded09fae30e9ef5b2520abeb22e26b7fab8264ef855af2ee4957d253f8dec90adc4b0ed4e4e45363466f1e96357ad0e910b362e8ab7dbe52b1767d0cb3202095cb33976870a4d2669cf7b68bbdf02222b767ee82faabc7d136c8b03108c85698e72b9644b7e9238cb35c6b7d30574992a763222ee63eb
                    12ab6263b8e12db54b8499294268157c3e3b230edfde1662a135175235243eb197cc25f58fe080a7e698a36c7036ca89dfe021b2ad7c39ea7a5eeaca2c14713c676def8da902a4046b449d119fa96ce9897d94aab876ea1e5d9670319a324270d9a3037c85b2b242a3b38b0dc2ca8506b7e82d07456f0dc0846d7359146db56200b25985e15d6f74cb0b9e52c65fc51ecfe79aab8723e5a736616d0b0cab3922eb8f965615c9f47c9f35aa1ef6c852430598dc6608f5c2853b1abe4dc0b5ae1c62a2c73b2fa9d0cf7ee7a939a7ec2a2e8c96b2e72f8c8de281d190d09c46b842d344720281aff6fd62f76e82db68e44eaf23a3199fccafe95920213bd7d424465dae9e6d76df2e07a6794012953a810bfde18058526bcb46b30a52c9ba8e1cc86d7975defd898795f7a9d46adb54ed86620029018fed7617b0acf0633f6ecc8f0e4c10748e24fd5b24c12c895cc8f59bff1bd52e11d38202bfe9909166b1fca6336d4c922cc06769d4ec3f9f56fea5cc46aa81ade217cc79797993ee1bf348fe322796e250bbe69783bc53da2ee566f134ced10bca5de93f6046a1ffbe1d9119550d58979ef5366c1df26f1cebe62959a74f2081ed9c9401eea7e2ad3a84cd68bcb49c6f62bcad50aa7722eca44d8fdc87a12fa3f00481b973ed2281680b486548c1d380cbfd68ddf3894c0e1f00509802d9ef5aa2260ed0b4519172d51fd56bfdc929835b89f69e23318ec489af660f8d99485274eed9b158be07cc09331f6af3f0041d1074b141a694c5ca891874466d753eca193dd56688f875f26da8075c4f8a294019c5d7fe1e1d2bd8c90157f4fe781f47edb6c11786b43ebcbdb647f0f6370b9362af85acad076a683e97f9f5803ecb4e3fb3fa7d780b401ab24771b88e228c1cff03692a6419b29ab0b1a0da783786373a3375b1d3a7450387a19e12033e690a64c9c7e72c760ec68d2753f0759d2fcbd32196e324b61402d21cd75c98417a2d489852796b0ea721b0be7f5d3fdaac61ded56d4c5eb3612dd1fe44edf2bf7e2a1ebadd5f5954ed25fa7149184f0d0fbdd4231e1708d536646c248c7f981de3c055643a67f0f1b26cb095a9c5c16bae73f1a5c58350db0ef70b17674cb5d6d68ed64c4fb129d43107591917d293630661b51c6e36ed564db3a6a44d0db6500519677082b3e94b993c6bdcd9ea0ab10bbb4c16384df3089c633297e3e346bc2ed04227088f8337adbbc66c5d2ddb72ebd33165be9bc1a0dc00af2d89cd698708c5603d5ed9da7a69ed995f19bcfbacdf7947e3b81277f83551e44548c13165729261a559f51cc1e65150b2824aa93a21427b162411dc2175e2786f2af46bf70089b3bf4d0c84eb25cbc82581a24489bcb9ef04b7daf60db18acef2531bc113ede9181bb8f6d27fc49360603e19c5ee6187eda7dbb024e6f423ed7027b7af1f03c828ae80611d8dc4be197aa7042829720bcd783c6f8613d5987d14be683a9b8b67a6ac36b1012e3045894d8e9487d45cfe105ffe6a45ac3d3da03bff4bd96ee7b24b8f5f54a57033b62e2858abebb3c421101b1142653d43bee58178593df14318b41e325b3f040f607ac2c54bc6ab34c1f0b35ef91422c24f05eea8a9c4491ee990240aa06931377940e805cd807fbf551f3eb0de41c15672d6e0c6317d09553eb25a7de7a105ffa6f68af051363bf191d8dc3e921d8bd2ac624a63cb1c4c895963c00a853dd84baf667370712bd59b5fd0217f39cd5713ccd56b8902437b1a7dd14e291e3d1aea77eedd3c18fc13822dc4ce4becc6c0cd1449f4d507f17ec2eddf3e7e8d6b18e24f2b0961bf7784cfd36eab671eadb9da79269ab3d447a41cb0af7a48e4335b4813dfe3f79ae3c634b3e113ef87f1d2a14ba3bbaa19b52251931286a4aaa452a335a6398c510824f54cf3b5875de35b04af79c2aa2f32d83a2814f383b9f298fc2241675bd25e58ca4e4d205cafcc31dd74e5c8acb69790ea0a757fa4e55a82167179872bfed41837753b0b96f1df30c2a48b4260e5cab8c38eca7041a54d7b46f7cf89eec3c243ca9e05f2fcb36d8b9d0f8663825def1cf5502871dbf77ec2c65435a96eb7fa1ccbf4749569bd103eae94af3a404ce284db32b56aa9775118f5439499f6b41dca6d91c962f71ea8fbcebbdd8ec3c6e9c4d8659aebf233b5b4f3fadd4430cdb24b4e28fb7ffdf066be8b42638b39334d51793f72c505f4f8816c327c548dbd968ce0b9ccf3eeaad0cdd74cb72b1b62
                    494d1013197e199a2d2c8bf9611c21cc731d78a2cd4c8d840651801e8f817134a60902629aa161d196c5fe52721e9b69b47d39583bc86d1b3a542aa2dd25514d83c5c17034b13515c24402a779212f3faa575b4835dc63c89a13b7c5f9d71d8a21a66959cffcffa876c14248cc70bd1809746f7f1109f8e90b50bc37671d1e1a1c1ee9b5dd423195c87251271d34021a4036f70b30e33aa6f18e5ba04d8406854ac8cac8a3121af36cc5d579d0ecf38b7bf5fee46fe0ce526703a73a7e2e6e87adce70fa59636880e7b2e9e2c4ba7475ffcdde17045066bec4a5bd3bb5910750f2929ecc2501109626f5864ea0fee45e134c7c7c33a7bead0e19995e6ee7fbe9a3604dfae297a85bdf229e35267f03920099cb941fb1e31e8d9782d778492228c7a0ccc00224bd7dff31c730e5fabb0ff843679248fee319aa31f3c461bdd92012a4f7d8db60b5f00d542615470d9f6709058402523c023a2c3682c58b7711f12bccaa4ce58c6ac157ed468b960af9edd303c5cfd2420ba0bf83e84b291c643477e6584ce659bd23255097192cdbdd5de18292727c2d098c91a900beac7779225539be6136fc2873fc545ba79076f2c609bb8265b7728487a9d9b0e779c38415645dcfb919e61a5df0024a25eae36dda628b374211d3843afb1ecf35fabba0b9c0466ffbce5c64580fcf87683a0ebbb3b456105d329ea036f42b2d8b1c284cc3bd1a4634495f1f473943c28b7947f9d971b795b37ed5cda9dcb819c33ee07da632d8e940cc3ebdb3e9b07d88aef25f970608a5ca95f042c2b0065c2f946f1d74b661efc5bfe75308c77827d76f79bfbbd8c439e0125d1d77b359dd123e460c48330e72f1d798fd2e10250be4121659c73800e7250bcd12f8450c6a2e4b7362147bf20afdd1d4db7a28b3e360e5aafac48ce0c4dcb42a45597d5551a60c5c85e639c38b358fb8ab7e6ed30f882ca21e5e4a09ad84a1b975c876320e2296f016f146bec0105a7f4c169b34df4380ecf7ae33099682f06a58189d0d85361fbf670fd6dbcfd0261bf6c8f121cb322c89c70e18ecbc02156eb5fa57b4a979d5d51b4d46e9579c3860934084ce365b347b23751ea614c66f0cbe7060c9651c69c5e0d0154bf7795a8fb436c25e697474e0c868152353eb85abcb75be0343ba9c8233b1f02a2bd286b5a04e8347f60e5c8393db21dd9fbcae4447a1459723cef240dfb9d9725a8bd58e63b173ffe1384d219de170541f307557f1ffec256f93aa1d4c6c4e9e443f92f596cc4f3f91a174dbcd7419c304b03f99c0e499181de0345e6de63f0d962a99d843881916298aaea03d001e09b43aafefb4d3b62846c0c121cfe8a52ad666c87690906695c04a25a9b1dbc894b8703311f5b7363f56ad6e22efd3365206857307a956fc89ba83260df311b07b3f1f37580dc18fef152fc30f9983101b6847e3e8aeb3a3e0a28dabaae2d705de3d49b168c7eda21da8e4e11604a45464ae3792a5b6247c06fce1a3da925da14f2a7d3c1f4931137b1064773d063153de334c1ec2669da7f3b5cdd04863737bc8cba896b0cd91757872135195ba4605b39bfeae67373f1654faaa4ee392357c148481d6b051067f4e71a8c8ab485ef64c3cecd3632b78eb2dcad3486114832cf1a598807f5398b5ccd4f90766ebdc3952abd2dde12f4e2905b2f8240eaf026db83d16e9dafdcc1009f13a1150078d3b0937acf9e6998eddad98595dcbea26270bc639dbe991f57a268d111c3a28a34580e80a8524ba6c115c8e3f4e8dbdc3681b2936390d6785ff04eba33d37dae5d31c7ad70fd0be948691e9ec72efea0e129b7932fdcd90ee353f3527ca8307c3deec330f98cd1c0bf3fa3031292bf493efa0aec7e9fb8844b0a71c3f5249b54d931aa7fecaedd3b91763da3eb2fbc534dfea67ae3b2d7f67afb19032c71934499fbff0d0f5e823689a40c415271de3d4c1068b7331974a447c9432e2d5095371bbc6bf98d64669c17d005022f2b0b1caf0721ea8c88145793dbe1a0814524167b92a95e8affb1ca6a8f5fe938a8d66b8cd5395d7f2cd6cff97bbfe261ae75c8e8ac01ad5828b8042d29a05589c3289c36fccd29679646d63488a209b0007dec9f94dbc362d2ba54df477c77e41bbb2a6d678790b0839640e24a007662ce2517de0711da53229e8d2cdfde355a50e972ddf23394f914e9b9bc1f2bfc2050c5e6d5da8581cee2472e51bc8b2c047bf815766580d8f70ca24941adb83533c532ba6
                    d5c2f33be22b050ff9bd426688fe60d5c6bad4d5acf2772c153d404df98f1aca338c06ed455ec988e5a321a34e7b8fad2f187dfd336657b1d0c7ac8d60a0246bbdc28151155e20f0f28c29067283a13cad89d6b98bcd2feca0fdc3510ca25363513348013a2b37d70f3dd035afe63d220d50e8a6929d8722ee0c99b24c8facb1904387f5f377333bcdaada5e5e9bd15ad53095f780826d99f8036df89f91d4f222f6ce38eb65ddf682938810a3a6ef620c674af91adcaec72a94437a015583ebeda6011c41da02b570729d9cc8ec9051f7a2524dc42b981d57644c33fbeaa4746309dc21bbde6754ad9a6adf52d34c303b3c48a2de19f7600b582aa685318d3bf9cf71920cc958baab87d98a52acf364d6d9babaf6c4478d10a5996d144f2bc9a2c2ebaf544a84632475bb070435c726eef4e4f05e356543df6494c59cd981977ac277ac601e8712f8edbf83413b27ddb47c6b4f6b1826ec5948078d60241328d3a8114c762cee17a3cf1f7c91d8361c5d8532d315403aecefc9b68ef787abd81d28cf2811e84fac863a0fc4810c320c57a1aedd8dc59426009cf7ddc502d7da5c22ffe20ab4876ef376a292f9fd6a8094e0b8ae79c78d83fe7ce7c269f4d621fe61f7a19e3a7c9e31192c5cbf7b7345a1082fff23c2343e83e7a03646e6df52f24891ef03fd0dc75d32e80d64df0a693e0c4818c2284084a053e7e0e1bb42788f51ee2d867b930caa6aecb21a18a87e10c0b202eec4d9c61c583c72a3474b65a8491715214455a9760baba23239091fa496cdefab810637fd52983a5a671e0dbad37aff5e201bf57b187665cf43e6450cfed7a7ef589c0bd08c42c94805d159bca74b485521be3fa5012f6b090c8c1fbf2b6e6df2f5dd9bd0ebb73a4eb1fa10b5e19903045035bd56c4009d82bd4422e15e5d0cd82b341d6799d5c6efa1e91e1589edc8e37d931417fa0515ff3faee5fcbdc182d5be09ba05033ef75590d737196704f5d26a85f06930ad5146f26e5a08ee830833759c1f0fcaf3155d670b39a20af6f89290f98c9ea295ed87718ae5e41b719c4912a6ebc8add07ab40d1fd69279d9b6373e61f66cf2616ec9d4c73faea6707f24271ee68992f40a408a6e12070ec40467518659be5401f6a2ce2a06cb59f33382b98a50d54a5e48465aa3f34004bb7ecc34ffc94a9e7b057c8a02d7bec2915cbcb7e33f8f7e4ab34e29f4c62e580b891221e8b272dab26fd7edcef983aaefa0c2d0ab69da59050c4472232c079d3c50cc93a961271ad0e8fdb8672f9259ba27fe0f3ddf2c82c80587dd76f09343077a991fa2d8bc79bd238acd99a60b64d8c54c054f5996cd308dab413ce1884dcf007fc0c5267fe91b54a9006d38c1fb249569b10eec8123392a210b1f9dfb1ac0f3c8b28a0ca62bc3987a99bd1175b36051558f76e9478f4e91874a74ed39595560e4c7ad91c3253decc3476b35052f7c35f84d2ee807597be06025d1186189ff1f7f0d96bf46159c033ce241d42a6ead38c20358499703b64ca23e7633105c8bf9012a81e214a5243d41ea6a731e4a746e048a7241a0632283278183a8da9ce8d968a5f0aeaffd5d206f59bd47f226c6b91548be71fdab9d76f247375614765e4f8cb0872a72ae0ae544238abfe037d76ba2577d22301dddda4947e8e332d6ea4ff5a1422148a4adeffa0bd6eb490837ebe02e80de9d50e0d59153875a3aca17bf38599e059c203da85c4b8db62e8d01a2c5242ee789faa5b61dde468812aef08659777510d396dc18e5c26c64060f277514fe9b79ed1e2b1575156c156c7ca5af561a99700bd596d6238bab7fed37264bdf30e892f4404f3e1a3973b900c54add0b1618ecd1e7918eb198e6f17649f0b8533beb7db824ad9d4847dca8af0a7420d8bfd06980642c948cc3f77c86050472335bf1764363b822c5567e0628805748ba63d4da90bb4c5a21504aba9deb46ce12d8c9338a245ca93fef347dcf877c56da005bfb57cf73e18c61123b129a3c8b40fa45404244a754ab637c7f273102a5a158e8a8974fe2e91c980dc2558db30a5e604aebbc69456778f2a11e06beb3b8935ac583704e62d337aa989894cd4ee353aae940333922580314cc64d7eca8c6cb38f5c8c369e73720acab4b50e4cdbdfe9e1874e77861b3a7fae0aa913b0faea122381c56b8fa72f5732ec4c74e8fff422f71c3afb3e79e609b3f9b8d23b3010b90da1e91bd18eaf4626b95af4b15b951f05a90b4804bdd4a6789ea",
                    "key" => "5754536430253400406320361241461064057212631024450254546567552177001211134270230044517467264626005374700077017463301426500420240035120731001555743444121114513663"
                ],
                "orbit_data" => [
                    "available" => false,
                    "signal" => 4,
                    "load" => "high"
                ],

            ];
        } else {
            $obj = (object) [
                'status' => "Could not establish connection with satellite " . $request->input('sat') . ".",
                "status_code" => 5050,
            ];
        }
    

    return json_encode($obj);
    });

    Route::middleware('api')->get('/propulzion/phoenix', function(Request $request)
    {
        
    $obj = (object) [
        'name' => "The flames have sparked. I'm on fire.",
        'quoter' => "Tek Xylo",
        'quote' => "I've made it out alive! The flames are now buring me to ascend higher. Somewhere New.",
        'day' => 2,
        "status_code" => 5000,
    ];
    

    return json_encode($obj);
    });

    Route::middleware('api')->get('/propulzion/apod', function(Request $request)
    {

        $curlSession = curl_init();
        curl_setopt($curlSession, CURLOPT_URL, 'https://api.nasa.gov/planetary/apod?api_key=19hBGHBtzbWcYKsCr0llqmXpgwLnRDyhf1Rp72nQ&date=' . $request->input('date'));
        curl_setopt($curlSession, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
    
    

        return curl_exec($curlSession);
    });

    Route::get('/apiping', function()
    {
        $obj = (object) [
            'name' => 'LynxAPI',
            'version' => 2.2,
            'impactversion' => 1.2,
            'pingstatus' => 2000
        ];
        

        return json_encode($obj);
    });
});
