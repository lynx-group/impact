<?php

use App\Http\Livewire\Account\Accsettings;
use Illuminate\Support\Facades\Route;
use Laravel\Fortify\Fortify;
use Laravel\Fortify\Actions\EnableTwoFactorAuthentication;
use App\Models\DiscordVerification;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/* AUTHENTICATION */

Route::get('/login', App\Http\Livewire\Login::class)->name('login');
Route::get('/register', App\Http\Livewire\Register::class)->name('register');

Route::get('/test', [App\Http\Controllers\LoginAuthenticationController::class, 'authenticate']);
Route::get('/verifydiscord', [App\Http\Controllers\DiscordVerificationController::class, 'index']);
Route::get('/discorddata', [App\Http\Controllers\DiscordVerificationController::class, 'getdata']);

//Fortify::verifyEmailView(function () {
//    return view('auth.verify-email');
//});

Route::get('/discord', function()
{
    $count = DiscordVerification::all()->count();
    return view('impact.discord')->with([
        'count' => $count
    ]);
})->name('discord-impact');

Route::get('/verify/{id}', App\Http\Livewire\Verify::class);
Route::get('/checkid/{id}', function($id)
{

    if(DiscordVerification::where('discord_id', $id)->get()->count() == 1) {
        $obj = (object) [
            'found' => true,
            'token' => DiscordVerification::where('discord_id', $id)->first()->token
        ];
    } else {
        $obj = (object) [
            'found' => false,
            'result' => DiscordVerification::where('discord_id', $id)->get()->count()
        ];
    }
    

    return json_encode($obj);
});


/* IMPACT */
Route::domain(env('APP_URL'))->group(function ()
{
    Route::get('/', App\Http\Livewire\Frontpage::class)->name('frontpage-impact');
    Route::get('/home', App\Http\Livewire\Dashboard\Dashboard::class)->name('home-impact');
    Route::get('/account/settings', App\Http\Livewire\Account\Accsettings::class)->middleware('auth');
    Route::get('/space/infrastructure', App\Http\Livewire\SpaceIS::class)->name('space');
    Route::get('/deploy', [App\Http\Controllers\DeployController::class, 'deploy']);
});

Route::domain('downloads.' . env('APP_URL'))->group(function ()
{
    Route::get('/', App\Http\Livewire\Downloads::class)->name('downloads');
});

Route::domain('raptor.' . env('APP_URL'))->group(function ()
{
    Route::get('/', App\Http\Livewire\Raptor::class)->name('raptor');
});


/* ADMIN PANEL */
Route::domain('admin.' . env('APP_URL'))->group(function ()
{
    Route::get('/', function () {
        return 'admin route';
    });
});